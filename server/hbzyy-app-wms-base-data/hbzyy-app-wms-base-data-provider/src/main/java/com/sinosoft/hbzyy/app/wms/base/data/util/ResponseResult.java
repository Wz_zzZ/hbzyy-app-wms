package com.sinosoft.hbzyy.app.wms.base.data.util;

import java.util.Map;

/**
 * 网易云返回值
 * 
 * @author Administrator
 *
 */
public class ResponseResult {
	/**
	 * 状态码
	 */
	private String code;

	/**
	 * 返回信息
	 */
	private Map<String, String> info;
	
	/**
	 * 错误描述
	 */
	private String desc;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Map<String, String> getInfo() {
		return info;
	}

	public void setInfo(Map<String, String> info) {
		this.info = info;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
