package com.sinosoft.hbzyy.app.wms.base.data.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sinosoft.hbzyy.app.wms.base.data.model.DepartmentEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.DistrictEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.ParentDeptDto;
import com.sinosoft.hbzyy.app.wms.base.data.model.ParentDeptEntity;

/**
 * 
 * @author 赵飞
 * @description:科室大类持久层
 * @date：2018年3月20日下午4:10:05
 * @type：DepartmentMapper
 * @version: 1.0
 * @company:中科软
 */
public interface ParentDeptMapper {
	

	/**
	 * 新增科室大类
	 * @param parentDept
	 * @return
	 */
	public int createParentDept(ParentDeptEntity parentDept);

	/**
	 * 更新科室大类
	 * @param parentDept
	 * @return
	 */
	public int updateParentDept(ParentDeptEntity parentDept);
	
	/**
	 * 根据条件查询科室大类
	 * @param orgCode
	 * @param code
	 * @param status
	 * @return
	 */
	public List<ParentDeptEntity> retrieveParentDept (@Param("orgCode")String orgCode,@Param("code")String code,@Param("status")String status);
	
	/**
	 * 根据条件查询科室数量
	 * @param name
	 * @param code
	 * @return
	 */
	public int retrieveParentCount(@Param("name")String name,@Param("code")String code);
	
	/**
	 * 删除科室大类
	 * @param id
	 */
	public void deleteParentDetp(String id);
	
	/**
	 * 根据id查询科室编码
	 * @param id
	 * @return
	 */
	public ParentDeptEntity selectParentDeptById(String id);
	
	/**
	 * 根据科室编码查询科室编码
	 * @param code
	 * @return
	 */
	public int updateParentCodeByCode(String code);
	
	public List<ParentDeptDto> retrieveParentDeptDto (@Param("orgCode")String orgCode,@Param("orgId")String orgId,@Param("deptName")String deptName);
	
	public ParentDeptEntity retrieveParentByParentCode(@Param("parentCode")String parentCode,@Param("orgId")String orgId);
}
