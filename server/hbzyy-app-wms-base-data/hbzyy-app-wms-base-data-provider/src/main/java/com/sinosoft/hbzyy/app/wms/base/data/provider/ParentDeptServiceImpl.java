package com.sinosoft.hbzyy.app.wms.base.data.provider;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.curator.shaded.com.google.common.collect.Lists;

import com.sinosoft.hbzyy.app.wms.base.data.api.ParentDeptService;
import com.sinosoft.hbzyy.app.wms.base.data.dao.ParentDeptMapper;
import com.sinosoft.hbzyy.app.wms.base.data.model.ParentDeptEntity;
import com.sinosoft.hbzyy.framework.util.ResultHelper;
/**
 * 科室大类的操作实现类
 * @author zhaofei
 *
 */
public class ParentDeptServiceImpl implements ParentDeptService {

	private ParentDeptMapper parentDeptMapper;
	
	/**
	 * 新增大类科室分类
	 */
	@Override
	public Map<String, Object> createParentDept(ParentDeptEntity parentDept) {
		//添加验证是否有重复的分类存在！
		if(parentDept!=null) {
			int resultCount = parentDeptMapper.retrieveParentCount(parentDept.getName(), parentDept.getCode());
			if(resultCount>0) {
				return ResultHelper.buildErrorResult("该分类已存在");
			}else{
				parentDept.setId(String.valueOf(UUID.randomUUID().toString()));
				Calendar now =  Calendar.getInstance();
				parentDept.setCreateAt(Long.valueOf(now.getTimeInMillis()));
				parentDept.setStatus("0");
				parentDeptMapper.createParentDept(parentDept);
				return ResultHelper.buildSuccessResult("添加成功");
			}

		}
		return ResultHelper.buildErrorResult("添加失败");
	}

	/**
	 * 更新科室大类信息
	 */
	@Override
	public Map<String, Object> updateParentDept(ParentDeptEntity parentDept) {
		if(parentDept!=null) {
			parentDept.setUpdateAt(Calendar.getInstance().getTimeInMillis());
			parentDeptMapper.updateParentDept(parentDept);
			return ResultHelper.buildSuccessResult("操作成功");
		}
		return ResultHelper.buildErrorResult("操作失败");
	}

	/**
	 * 根据机构编码，大科室编码及状态查询科室大类
	 */
	@Override
	public Map<String, Object> retrieveParentDept(String orgCode, String code, String status) {
		List<ParentDeptEntity> parent = Lists.newArrayList();
		try {
			parent = parentDeptMapper.retrieveParentDept(orgCode, code, status);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("查询分类错误"+e.getMessage());
		}
		return ResultHelper.buildSuccessResult(parent);
	}

	/**
	 * 删除大科室（同时清除科室的）
	 */
	@Override
	public Map<String, Object> deleteParentDept(String id) {
		if(StringUtils.isBlank(id)) {
			return ResultHelper.buildErrorResult("请选择要删除的父科室");
		}
		ParentDeptEntity parentDept= parentDeptMapper.selectParentDeptById(id);
		try {
			
		     parentDeptMapper.deleteParentDetp(id);
			//清除小类里的 方法
		     if(!StringUtils.isBlank(parentDept.getCode())) {
		    	 parentDeptMapper.updateParentCodeByCode(parentDept.getCode());
		     }
			return ResultHelper.buildSuccessResult("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("删除失败");
		}
	}
	
	public ParentDeptMapper getParentDeptMapper() {
		return parentDeptMapper;
	}

	public void setParentDeptMapper(ParentDeptMapper parentDeptMapper) {
		this.parentDeptMapper = parentDeptMapper;
	}

}
