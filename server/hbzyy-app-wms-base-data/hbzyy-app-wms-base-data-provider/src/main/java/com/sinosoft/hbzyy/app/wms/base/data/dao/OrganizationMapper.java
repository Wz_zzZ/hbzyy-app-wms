package com.sinosoft.hbzyy.app.wms.base.data.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sinosoft.hbzyy.app.wms.base.data.model.OrganizationEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.DistrictEntity;

/**
 * 
 * @author 刘富鹏
 * @description:机构信息持久程
 * @date：2018年3月20日下午4:11:46
 * @type：OrganizationMapper
 * @version: 1.0
 * @company:中科软
 */
public interface OrganizationMapper {
	
	/**
	 * 创建机构信息服务
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	public void createOrganization(OrganizationEntity organ);
	
	/**
	 * 修改机构信息
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	public void updateOrganization(OrganizationEntity organ);
	
	/**
	 * 依据机构编码查询机构信息
	 * @param orgCode 机构编码
	 * @return 机构信息
	 */
	public List<OrganizationEntity> retrieveOrganizationByOrgCode(String orgCode);
	
	/**
	 * 多条件查询机构信息
	 * @param organ 机构信息入参
	 * @return 机构信息集合
	 */
	public List<OrganizationEntity> retrieveOrganization(@Param("orgName")String orgName,@Param("orgCode")String orgCode,
			@Param("level")String level,
			@Param("currentPage")int currentPage,@Param("rows")int rows);
	
	/**
	 * 通过机构编码物理删除机构信息
	 * @param orgCode 机构编码
	 * @return 标识带数据
	 */
	public void deleteOrganizationByOrgCode(String orgCode);
	
	/**
	 * 通过机构编码更新状态（逻辑删除机构信息）
	 * @param orgCode
	 * @param status
	 * @return
	 */
	public void updateOrganizationByOrgCode(String orgCode,String status);
	
	/**
	 * 查看机构条数
	 * @return
	 */
	public int retrieveOrganizationCount(@Param("orgName")String orgName,@Param("orgCode")String orgCode,
			@Param("level")String level);

	public void createDistricts(List<DistrictEntity> schools);

	public void updateDistrict(DistrictEntity schools);

	public void deleteDistrictById(String id);
	
	public void createDistrict(DistrictEntity district);
	
	/**
	 * 删除院区
	 * @param orgCode
	 * @param districtId
	 */
	public void deleteDistrictByOrgCodeAndDistrictId(@Param("orgCode")String orgCode, @Param("districtId")String districtId);
	
	/**
	 * 查询院区信息
	 * @param orgCode
	 * @param districtId
	 * @return
	 */
	public DistrictEntity retrieveDistrictByOrgCodeAndDistictId(
								@Param("orgCode")String orgCode,
								@Param("districtId")String districtId,
								@Param("districtName")String districtName);
	
	public List<DistrictEntity> retrieveDistrictByOrgCode(String orgCode);
	
	public List<OrganizationEntity> retrieveAllOrg ();

}
