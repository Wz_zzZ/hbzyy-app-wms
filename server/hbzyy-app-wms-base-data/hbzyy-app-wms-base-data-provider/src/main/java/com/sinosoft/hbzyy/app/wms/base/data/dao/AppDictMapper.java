package com.sinosoft.hbzyy.app.wms.base.data.dao;

import java.util.List;

import com.sinosoft.hbzyy.app.wms.base.data.model.AppDictItemEntity;

/**
 * 
 * @author 刘富鹏
 * @description:字典数据接口
 * @date：2018年3月27日下午1:29:29
 * @type：AppDictMapper
 * @version: 1.0
 * @company: 中科软
 */
public interface AppDictMapper {

	List<AppDictItemEntity> retrieveItemByDid(String did);

	List<AppDictItemEntity> retrieveItemByPcode(String pcode);
	
}
