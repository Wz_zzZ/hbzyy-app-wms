package com.sinosoft.hbzyy.app.wms.base.data.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.google.common.collect.Lists;
import com.sinosoft.framework.core.common.utils.security.DigestUtil;
import com.sinosoft.framework.core.common.utils.string.PymUtils;
import com.sinosoft.framework.core.common.utils.string.StringUtil;
import com.sinosoft.hbzyy.app.wms.base.data.api.DoctorUserService;
import com.sinosoft.hbzyy.app.wms.base.data.dao.DoctorUserMapper;
import com.sinosoft.hbzyy.app.wms.base.data.model.DoctorDeptEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.DoctorUserEntity;
import com.sinosoft.hbzyy.app.wms.base.data.util.IdcardValidatorUtils;
import com.sinosoft.hbzyy.app.wms.base.data.util.NeteaseClient;
import com.sinosoft.hbzyy.app.wms.base.data.util.ResponseResult;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;
import com.sinosoft.hbzyy.framework.util.ResultHelper;

/**
 * 
 * @author 刘富鹏
 * @description:人员信息服务实现类
 * @date：2018年3月20日下午3:34:08
 * @type：DoctorUserServiceImpl
 * @version: 1.0
 * @company:中科软
 */
public class DoctorUserServiceImpl implements DoctorUserService {

    private static final Logger logger = Logger.getLogger(DoctorUserServiceImpl.class);
	public static String salt = "hbzyy-app-server-wms";

	/**
	 * 人员信息
	 */
	private DoctorUserMapper doctorUserMapper;

	public void setDoctorUserMapper(DoctorUserMapper doctorUserMapper) {
		this.doctorUserMapper = doctorUserMapper;
	}

	@Override
	public Map<String, Object> createDoctorUser(DoctorUserEntity doctor) {
		try {
			/*String id = UUID.randomUUID().toString();
			doctor.setId(id);*/
			doctor.setNamePy(PymUtils.convert2Py(doctor.getName()));
			if(!StringUtil.isBlank(doctor.getIdCard())) {
				doctor.setGender(IdcardValidatorUtils.getGender(doctor.getIdCard()));
				doctor.setAge(IdcardValidatorUtils.getAge(doctor.getIdCard()));
			}
			doctor.setCreateAt(System.currentTimeMillis());
			doctor.setStatus("1");
			String accid = UUID.randomUUID().toString().replaceAll("-", "");
			NeteaseClient.creat(accid, doctor.getName());
			doctor.setAccid(accid);
			List<DoctorDeptEntity> doctDepts = new ArrayList<DoctorDeptEntity>();
			// 如果是多科室的进行截取并进行添加
			// String deptCode = "2_pf,3_pf,4_kf";
			
			//String deptName = "光谷院区_皮肤科,花园山院区_康复科"
			if (doctor.getDeptCode().contains(",")) {
				String[] deptCodes = doctor.getDeptCode().split(",");// [2-pf,3-pf,4-fk]
				for (String deptCode : deptCodes) {
					String[] deptAndOrgCode = deptCode.split("_");// [2,pf]
					if (deptAndOrgCode.length > 0) {// true
						DoctorDeptEntity doctDept = new DoctorDeptEntity();
						doctDept.setDocId(doctor.getId());
						doctDept.setOrgCode(doctor.getOrgCode());
						doctDept.setDeptCode(deptAndOrgCode[1]);
						doctDept.setStatus("1");
						doctDept.setDistrictId(deptAndOrgCode[0]);
						doctDepts.add(doctDept);
					}
				}
				// for(String deptCode:deptCodes) {
				// DoctorDeptEntity doctDept=new DoctorDeptEntity();
				// doctDept.setDocId(id);
				// doctDept.setOrgCode(doctor.getOrgCode());
				// doctDept.setDeptCode(deptCode);
				// doctDept.setStatus("1");
				// doctDepts.add(doctDept);
				// }
				doctorUserMapper.createDoctorDept(doctDepts);
			} else {// 表示该医生一个院区的一个科室
				String[] deptAndOrgCode = doctor.getDeptCode().split("_");
				if (deptAndOrgCode.length > 0) {
					DoctorDeptEntity doctDept = new DoctorDeptEntity();
					doctDept.setDeptCode(deptAndOrgCode[0]);
					doctDept.setDocId(doctor.getId());
					doctDept.setOrgCode(deptAndOrgCode[0]);
					doctDept.setDistrictId(deptAndOrgCode[0]);
					doctDept.setStatus("1");
					doctDepts.add(doctDept);
					doctorUserMapper.createDoctorDept(doctDepts);
				} else {
					return ResultHelper.buildErrorResult("未选择任何科室信息");
				}

			}
			doctorUserMapper.createDoctorUser(doctor);
			return ResultHelper.buildSuccessResult(doctor);
		} catch (Exception e) {
			return ResultHelper.buildErrorResult(e.getMessage());
		}
	}

	@Override
	public Map<String, Object> updateDoctorUser(DoctorUserEntity doctor) {
		try {
			doctor.setUpdateAt(System.currentTimeMillis());
			doctorUserMapper.updateDoctorUser(doctor);
			NeteaseClient.updateUserInfo(doctor.getAccid(), doctor.getName());
			return ResultHelper.buildSuccessResult(doctor);
		} catch (Exception e) {
			return ResultHelper.buildErrorResult("数据更新失败:" + e.getMessage());
		}
	}

	@Override
	public Map<String, Object> updateDoctorTextAndVideo(String id, String isText, String isVideo) {
		try {
			doctorUserMapper.updateDoctorTextAndVideo(id, isText, isVideo);
			return ResultHelper.buildSuccessResult("修改权限成功");
		} catch (Exception e) {
			return ResultHelper.buildErrorResult("更新权限失败：" + e.getMessage());
		}
	}

	@Override
	public Map<String, Object> updateDoctorStatusById(String id, String status) {
		try {
			doctorUserMapper.updateDoctorStatusById(id, status);
			return ResultHelper.buildSuccessResult("修改状态成功");
		} catch (Exception e) {
			return ResultHelper.buildErrorResult("修改状态失败：" + e.getMessage());
		}
	}

	@Override
	public Map<String, Object> retrieveDoctorByTelephone(String telephone) {
		List<DoctorUserEntity> doctors = doctorUserMapper.retrieveDoctorByTelephone(telephone);
		return ResultHelper.buildSuccessResult(doctors);
	}

	@Override
	public Map<String, Object> retrieveDoctorByName(String name) {
		List<DoctorUserEntity> doctors = doctorUserMapper.retrieveDoctorByName(name);
		return ResultHelper.buildSuccessResult(doctors);
	}

	public PageList<DoctorUserEntity> retrieveDoctorUser(String id, String orgCode, String name, String deptCode,
			int currentPage, int rows) {
		List<DoctorUserEntity> doctors = doctorUserMapper.retrieveDoctorUser(id, orgCode, name, deptCode, currentPage,
				rows);
		int count = doctorUserMapper.retrieveDoctorUserCount(id, orgCode, name, deptCode);
		PageList<DoctorUserEntity> pageList = new PageList<DoctorUserEntity>();
		pageList.setDatas(doctors);
		pageList.setPageNum(currentPage);
		pageList.setPageSize(rows);
		pageList.setTotal(count);
		return pageList;
	}

	@Override
	public Map<String, Object> retrieveDoctorByOrgCode(String orgCode) {
		List<DoctorUserEntity> doctors = doctorUserMapper.retrieveDoctorByOrgCode(orgCode);
		return ResultHelper.buildSuccessResult(doctors);
	}

	@Override
	public Map<String, Object> retrieveDoctorByOrgCodeAndDeptCode(String orgCode, String deptCode) {
		List<DoctorUserEntity> doctors = doctorUserMapper.retrieveDoctorByOrgCodeAndDeptCode(orgCode, deptCode);
		return ResultHelper.buildSuccessResult(doctors);
	}

	@Override
	public void deleteDoctorUserById(String id) {
		doctorUserMapper.deleteDoctorUserById(id);
		doctorUserMapper.deleteDoctDeptByUid(id);
		
	}

	@Override
	public Map<String, Object> retrieveDoctorAccidByDoctorCode(String doctorCode) {
		String accid = doctorUserMapper.selectAccidByDoctorCode(doctorCode);
		return ResultHelper.buildSuccessResult(accid);

	}

	@Override
	public String retrieveAccidByDoctorCode(String doctorCode) {
		return doctorUserMapper.selectAccidByDoctorCode(doctorCode);
	}

	@Override
	public Map<String, Object> retrieveDoctorByPinYin(String namePy,int curpage,int pageSize) {
		List<DoctorUserEntity> doctorUserList = Lists.newArrayList();
		int totalCount = 0;
		try {
			doctorUserList = doctorUserMapper.retrieveDoctorByPinYin(namePy,curpage,pageSize);
			totalCount = doctorUserMapper.retrieveDoctorByPinYinCount(namePy);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("数据库异常"+e.getMessage());
			return ResultHelper.buildErrorResult("查询错误，请稍后再试");
		}
		if(!doctorUserList.isEmpty()) {
			PageList<DoctorUserEntity> pageList = new PageList<DoctorUserEntity>();
			pageList.setDatas(doctorUserList);
			pageList.setPageNum(curpage);
			pageList.setPageSize(pageSize);
			pageList.setTotal(totalCount);
			return ResultHelper.buildSuccessResult(pageList);
		}
		return ResultHelper.buildSuccessResult(doctorUserList);
	}

	@Override
	public Map<String, Object> retrieveDoctorAllByName(String name) {
		// TODO Auto-generated method stub
		List<DoctorUserEntity> allList = org.apache.curator.shaded.com.google.common.collect.Lists.newArrayList();
		try {
			allList = doctorUserMapper.retrieveAllDoctorByName(name);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("查询失败，请稍后再试");
		}
		return ResultHelper.buildSuccessResult(allList);
	}

}
