package com.sinosoft.hbzyy.app.wms.base.data.util;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class NeteaseClient {
	/**
	 * 开发者平台分配的appkey
	 */
	private static String APPKEY;
//	= "c155b548167d43360c8d62e56066037c";
//		private static final String APPKEY = "1e2c014c040e811c98f34c57036dbabc";
	/**
	 * 开发者私钥
	 */
		private static String APPSECRET;
//		= "a0c38a9c8d81";
//		private static final String APPSECRET = "380ef7450545";
	
	private static final String TOKEN = "123456";
	
	public String getAPPKEY() {
		return APPKEY;
	}
	@Value("${netease.APPKEY}")
	public void setAPPKEY(String aPPKEY) {
		APPKEY = aPPKEY;
	}

	public String getAPPSECRET() {
		return APPSECRET;
	}
	@Value("${netease.APPSECRET}")
	public void setAPPSECRET(String aPPSECRET) {
		APPSECRET = aPPSECRET;
	}

	/**
	 * 创建网易云通信ID
	 * @param accid 
	 * @return
	 * @throws IOException 
	 * @throws JsonParseException 
	 */
	public static ResponseResult creat(String accid,String phone) throws JsonParseException, IOException {
		String url = "https://api.netease.im/nimserver/user/create.action";
		Form form = new Form();
        form.param("accid", accid);
        form.param("name", phone);
        form.param("token", TOKEN);
        Response r = doPost(url, form);
		return parseResponseResult(r);
	}
	
	public static ResponseResult updateUserInfo(String accid,String name) throws JsonParseException, IOException {
		String url = "https://api.netease.im/nimserver/user/updateUinfo.action";
		Form form = new Form();
        form.param("accid", accid);
        form.param("name", name);
        Response r = doPost(url, form);
		return parseResponseResult(r);
	}
	
	/**
	 * 更新并重新获取token
	 * @param accid
	 * @return
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public static ResponseResult refreshToken(String accid) throws JsonProcessingException, IOException {
		String url = "https://api.netease.im/nimserver/user/refreshToken.action";
        Form form = new Form();
        form.param("accid", accid);
        Response r = doPost(url, form);
		return parseResponseResult(r);
		
	}
	
	/**
	 * 发送post请求
	 * @param url 请求路径 form 请求参数
	 * @return
	 */
	private static Response doPost(String url,Form form) {
		WebClient client = WebClient.create(url);
		String nonce = getUUID();
		String curTime = String.valueOf((new Date()).getTime() / 1000L);
        String checkSum = CheckSumBuilder.getCheckSum(APPSECRET, nonce ,curTime);
		client.type("application/x-www-form-urlencoded").accept("application/json");
        client.header("AppKey", APPKEY);
        client.header("Nonce", nonce);
        client.header("CurTime", curTime);
        client.header("CheckSum", checkSum);
        Response r = client.post(form);
		return r;
	}
	
	/**
	 * 解析返回值
	 * @return
	 * @throws IOException 
	 * @throws JsonParseException 
	 */
	private static ResponseResult parseResponseResult(Response res) throws JsonParseException, IOException {
		ObjectMapper mapper = new ObjectMapper();  
		ResponseResult rr = mapper.readValue(res.readEntity(String.class), ResponseResult.class); 
		return rr;
	}
	
	/**
	 * 生成随机数Nonce
	 * @return
	 */
	private static String getUUID() {
		String s = UUID.randomUUID().toString();
		return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
	}
	
	public static void main(String[] args) throws JsonParseException, IOException {
		String url = "https://api.netease.im/nimserver/user/updateUinfo.action";
		Form form = new Form();
        form.param("accid", DigestUtils.md5Hex("13913692580" + "hbzyy"));
        form.param("name", "王珊珊");
        Response r = doPost(url, form);
        System.out.println(r.readEntity(String.class));
	}
}
