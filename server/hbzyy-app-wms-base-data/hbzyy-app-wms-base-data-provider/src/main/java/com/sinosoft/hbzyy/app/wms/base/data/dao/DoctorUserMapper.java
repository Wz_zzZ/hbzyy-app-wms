package com.sinosoft.hbzyy.app.wms.base.data.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sinosoft.hbzyy.app.wms.base.data.model.DoctorDeptEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.DoctorUserEntity;

/**
 * 
 * @author 刘富鹏
 * @description:人员信息持久层
 * @date：2018年3月20日下午4:11:01
 * @type：DoctorUserMapper
 * @version: 1.0
 * @company:中科软
 */
public interface DoctorUserMapper {
	
	/**
	 * 创建人员信息
	 * @param doctor 人员信息
	 * @return 标识带数据
	 */
	public void createDoctorUser(DoctorUserEntity doctor);
	
	/**
	 * 更新人员信息
	 * @param doctor 人员信息
	 * @return 标识带数据
	 */
	public void updateDoctorUser(DoctorUserEntity doctor);
	
	/**
	 * 更新是否有图文咨询和视频问诊权限
	 * @param id 主键id	
	 * @param isText 是否有图文咨询 0 没有 1 有
	 * @param isVideo 是否有视频会诊权限 0 没有 1有
	 * @return	标识带数据
	 */
	public void updateDoctorTextAndVideo(String id,String isText,String isVideo);
	
	/**
	 * 更新医生是否启用状态 
	 * @param id 主键id
	 * @param status 状态值 0 禁用 1启用
	 * @return 标识带数据
	 */
	public void updateDoctorStatusById(String id,String status);
	
	/**
	 * 更新医生是否启用状态 
	 * @param orgCode 机构编码orgCode
	 * @param status 状态值 0 禁用 1启用
	 * @return 标识带数据
	 */
	public void updateDoctorStatusByOrgCode(String orgCode,String status);
	
	/**
	 * 通过电话号码查询人员信息
	 * @param telephone 手机号码
	 * @return 标识带数据
	 */
	public List<DoctorUserEntity> retrieveDoctorByTelephone(String telephone);
	
	/**
	 * 通过姓名查询医生信息
	 * @param name 医生人员姓名
	 * @return 标识带数据
	 */
	public List<DoctorUserEntity> retrieveDoctorByName(String name);
	
	/**
	 * 通过姓名查询医生信息
	 * @param name 医生人员姓名
	 * @return 标识带数据
	 */
	public List<DoctorUserEntity> retrieveDoctorUser(@Param("id")String id,@Param("orgCode")String orgCode,@Param("name")String name,
			@Param("deptCode")String deptCode,@Param("currentPage")int currentPage,@Param("rows")int rows);
	
	/**
	 * 查询人员信息的条数
	 * @return
	 */
	public int retrieveDoctorUserCount(@Param("id")String id,@Param("orgCode")String orgCode,@Param("name")String name,
			@Param("deptCode")String deptCode);
	
	/**
	 * 通过机构编码查询人员信息
	 * @param orgCode 机构编码
	 * @return	标识带数据
	 */
	public List<DoctorUserEntity> retrieveDoctorByOrgCode(String orgCode);
	
	/**
	 * 通过机构编码和科室编码查询人员信息
	 * @param orgCode 机构编码
	 * @param deptCode 科室编码
	 * @return 标识带数据
	 */
	public List<DoctorUserEntity> retrieveDoctorByOrgCodeAndDeptCode(String orgCode,String deptCode);
	
	/**
	 * 删除人员信息
	 * @param orgCode
	 */
	public void deleteDoctorUserByOrgCode(String orgCode);
	
	/**
	 * 批量添加科室人员信息
	 * @param doctDepts
	 */
	public void createDoctorDept(List<DoctorDeptEntity> doctDepts);
	
	/**
	 * 依据科室编码更新关联表状态值
	 * @param deptCode
	 * @param status
	 */
	public void updateDoctDeptByDeptCode(String deptCode, String status);
	
	/**
	 * 删除人员信息
	 * @param id
	 */
	public void deleteDoctorUserById(String id);
	
	/**
	 * 删除人员科室对应信息
	 * @param id
	 */
	public void deleteDoctDeptByUid(String id);
	
	/**
	 * 根据doctorCode查其accid
	 * @param doctorCode
	 * @return
	 */
	public String selectAccidByDoctorCode(String doctorCode);
	
	public List<DoctorUserEntity> retrieveDoctorByPinYin (@Param("namePy")String namePy,@Param("curpage")int curpage,@Param("pageSize")int pageSize);
	
	public int retrieveDoctorByPinYinCount(@Param("namePy")String namePy);
	
    public List<DoctorUserEntity> retrieveAllDoctorByName(@Param("name") String name);
	

}
