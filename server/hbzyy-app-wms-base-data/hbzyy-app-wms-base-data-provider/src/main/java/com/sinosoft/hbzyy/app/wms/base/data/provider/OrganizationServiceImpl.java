package com.sinosoft.hbzyy.app.wms.base.data.provider;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.curator.shaded.com.google.common.collect.Lists;

import com.sinosoft.framework.core.common.utils.string.PymUtils;
import com.sinosoft.hbzyy.app.wms.base.data.api.OrganizationService;
import com.sinosoft.hbzyy.app.wms.base.data.dao.DepartmentMapper;
import com.sinosoft.hbzyy.app.wms.base.data.dao.DoctorUserMapper;
import com.sinosoft.hbzyy.app.wms.base.data.dao.OrganizationMapper;
import com.sinosoft.hbzyy.app.wms.base.data.model.DistrictEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.OrganizationEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;
import com.sinosoft.hbzyy.framework.util.ResultHelper;

/**
 * 
 * @author 刘富鹏
 * @description:机构信息服务实现
 * @date：2018年3月20日下午6:40:45
 * @type：OrganizationServiceImpl
 * @version: 1.0
 * @company: 中科软
 */
public class OrganizationServiceImpl implements OrganizationService {
	Log log=LogFactory.getLog(OrganizationServiceImpl.class);
	/**
	 * 机构信息mapper
	 */
	private OrganizationMapper organizationMapper;
	
	private DepartmentMapper departmentMapper;
	
	private DoctorUserMapper doctorUserMapper;

	@Override
	public Map<String, Object> createOrganization(OrganizationEntity organ) {
		organ.setId(UUID.randomUUID().toString());
		organ.setCreateAt(System.currentTimeMillis());
		organ.setStatus("1");
		organ.setOrgNamePy(PymUtils.convert2Py(organ.getOrgName()));
		organizationMapper.createOrganization(organ);
		return ResultHelper.buildSuccessResult(organ);
		
	}
	
	@Override
	public Map<String, Object> createOrganization(OrganizationEntity organ,DistrictEntity district) {
		String id=UUID.randomUUID().toString();
		organ.setId(id);
		organ.setCreateAt(System.currentTimeMillis());
		organ.setStatus("1");
		organ.setOrgNamePy(PymUtils.convert2Py(organ.getOrgName()));
		if(null!=organ) {
			organizationMapper.createOrganization(organ);
		}
		try {
			district.setId(id);
			organizationMapper.createDistrict(district);
			return ResultHelper.buildSuccessResult(organ);
		} catch (Exception e) {
			log.error(e.getMessage());
		   organizationMapper.deleteOrganizationByOrgCode(organ.getOrgCode());
		   return ResultHelper.buildErrorResult("创建机构失败,请重新创建");
		}
	}

	@Override
	public Map<String, Object> updateOrganization(OrganizationEntity organ) {
		try {
			organ.setUpdateAt(System.currentTimeMillis());
			organizationMapper.updateOrganization(organ);
			return ResultHelper.buildSuccessResult(organ);
		} catch (Exception e) {
			return ResultHelper.buildErrorResult(e.getMessage());
		}
	}

	@Override
	public Map<String, Object> retrieveOrganizationByOrgCode(String orgCode) {
		List<OrganizationEntity> organs =null;
		try {
			organs= organizationMapper.retrieveOrganizationByOrgCode(orgCode);
			return ResultHelper.buildSuccessResult(organs);
		} catch (Exception e) {
			return ResultHelper.buildErrorResult(e.getMessage());
		}
	}

	@Override
	public PageList<OrganizationEntity> retrieveOrganization(String orgName,String orgCode,String level,int currentPage,int rows) {
		int	count = organizationMapper.retrieveOrganizationCount(orgName,orgCode,level);
		List<OrganizationEntity> organs = organizationMapper.retrieveOrganization(orgName,orgCode,level,currentPage,rows);
		for(OrganizationEntity organ:organs) {
			List<DistrictEntity> schoolDis = organ.getSchoolDis();
			if(null!=schoolDis && schoolDis.size()>=0) {
				StringBuffer buffer=new StringBuffer();
				for(int i=0;i<schoolDis.size();i++) {
					if(i==schoolDis.size()-1) {
						organ.setSchoolDistrictName(buffer.append(schoolDis.get(i).getDistrictName()).toString());
					}else {
						organ.setSchoolDistrictName(buffer.append(schoolDis.get(i).getDistrictName()).append(",")
								.toString());
					}
				}
			}
		}
		PageList<OrganizationEntity> pageList=new PageList<OrganizationEntity>();
		pageList.setDatas(organs);
		pageList.setPageNum(currentPage);
		pageList.setPageSize(rows);
		pageList.setTotal(count);
		return pageList;
	}

	@Override
	public Map<String, Object> deleteOrganizationByOrgCode(String orgCode) {
		doctorUserMapper.deleteDoctorUserByOrgCode(orgCode);
		departmentMapper.deleteDepartmentByOrgCode(orgCode);
		organizationMapper.deleteOrganizationByOrgCode(orgCode);
		return ResultHelper.buildSuccessResult("机构删除成功");
	}

	@Override
	public Map<String, Object> updateOrganizationByOrgCode(String orgCode, String status) {
		/**
		 * 禁用医生
		 */
		doctorUserMapper.updateDoctorStatusByOrgCode(orgCode, status);
		/**
		 * 禁用科室
		 */
		departmentMapper.updateDepartmentByOrgCode(orgCode,status);
		/**
		 * 禁用机构
		 */
		organizationMapper.updateOrganizationByOrgCode(orgCode, status);
		return ResultHelper.buildSuccessResult("科室状态修改成功");
	}
	public OrganizationMapper getOrganizationMapper() {
		return organizationMapper;
	}

	public DepartmentMapper getDepartmentMapper() {
		return departmentMapper;
	}

	public DoctorUserMapper getDoctorUserMapper() {
		return doctorUserMapper;
	}

	public void setOrganizationMapper(OrganizationMapper organizationMapper) {
		this.organizationMapper = organizationMapper;
	}
	
	public void setDepartmentMapper(DepartmentMapper departmentMapper) {
		this.departmentMapper = departmentMapper;
	}

	public void setDoctorUserMapper(DoctorUserMapper doctorUserMapper) {
		this.doctorUserMapper = doctorUserMapper;
	}

	@Override
	public Map<String, Object> updateDistrict(DistrictEntity schools) {
			organizationMapper.updateDistrict(schools);
			return ResultHelper.buildSuccessResult(schools);
	}

	@Override
	public Map<String, Object> deleteDistrictById(String id) {
		try {
			organizationMapper.deleteDistrictById(id);
			return ResultHelper.buildSuccessResult(id);
		} catch (Exception e) {
		return ResultHelper.buildErrorResult("院区修改失败");
		}
	}

	@Override
	public Map<String, Object> createDistrict(DistrictEntity district) {
			district.setStatus("1");
			organizationMapper.createDistrict(district);
			return ResultHelper.buildSuccessResult(district);
	}

	@Override
	public void deleteDistrictByOrgCode(String orgCode) {
		organizationMapper.deleteOrganizationByOrgCode(orgCode);
		
	}

	@Override
	public void deleteDistrictByOrgCodeAndDistrictId(String orgCode, String districtId) {
		organizationMapper.deleteDistrictByOrgCodeAndDistrictId(orgCode,districtId);
	}

	@Override
	public DistrictEntity retrieveDistrictByOrgCodeAndDistictId(String orgCode, String districtId,String districtName) {
		return organizationMapper.retrieveDistrictByOrgCodeAndDistictId(orgCode,districtId,districtName);
	}

	@Override
	public Map<String, Object> retrieveDistrictByOrgCode(String orgCode) {
		List<DistrictEntity> list = organizationMapper.retrieveDistrictByOrgCode(orgCode);
		return ResultHelper.buildSuccessResult(list);
	}

	@Override
	public Map<String, Object> retrieveAllOrg() {
      List<OrganizationEntity> allList = Lists.newArrayList();
      try {
    	  allList = organizationMapper.retrieveAllOrg();
	} catch (Exception e) {
		e.printStackTrace();
		return ResultHelper.buildErrorResult("查询失败，请稍后重试");
	}
      return ResultHelper.buildSuccessResult(allList);
	}
}
