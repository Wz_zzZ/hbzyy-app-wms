package com.sinosoft.hbzyy.app.wms.base.data.provider;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.sinosoft.hbzyy.app.wms.base.data.api.AppDictService;
import com.sinosoft.hbzyy.app.wms.base.data.dao.AppDictMapper;
import com.sinosoft.hbzyy.app.wms.base.data.model.AppDictItemEntity;

/**
 * 
 * @author 刘富鹏
 * @description:查询字典实现类
 * @date：2018年3月27日下午2:01:04
 * @type：AppDictServiceImpl
 * @version: 
 * @company:
 */
public class AppDictServiceImpl implements AppDictService {

	private AppDictMapper appDictMapper;
	
	
	public AppDictMapper getAppDictMapper() {
		return appDictMapper;
	}

	public void setAppDictMapper(AppDictMapper appDictMapper) {
		this.appDictMapper = appDictMapper;
	}

	@Override
	public List<AppDictItemEntity> retrieveItemByDid(String did) {
		return appDictMapper.retrieveItemByDid(did);
		
	}

	@Override
	public List<AppDictItemEntity> retrieveItemByPcode(String pcode) {
		if(!StringUtils.isBlank(pcode)) {
			pcode=pcode.substring(0, 2);
		}
		return appDictMapper.retrieveItemByPcode(pcode);
	}

}
