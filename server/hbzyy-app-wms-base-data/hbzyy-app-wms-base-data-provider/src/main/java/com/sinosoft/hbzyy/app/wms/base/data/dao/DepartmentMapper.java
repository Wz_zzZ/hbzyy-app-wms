package com.sinosoft.hbzyy.app.wms.base.data.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sinosoft.hbzyy.app.wms.base.data.model.DepartmentEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.DeptDto;
import com.sinosoft.hbzyy.app.wms.base.data.model.DistrictEntity;

/**
 * 
 * @author 刘富鹏
 * @description:科室持久层
 * @date：2018年3月20日下午4:10:05
 * @type：DepartmentMapper
 * @version: 1.0
 * @company:中科软
 */
public interface DepartmentMapper {
	
	/**
	 * 创建科室信息
	 * @param depart 科室信息
	 * @return 标识带数据
	 */
	public void createDepartment(DepartmentEntity depart);
	
	/**
	 * 更新科室信息
	 * @param depart 科室信息
	 * @return 标识带数据
	 */
	public void updateDepartment(DepartmentEntity depart);
	
	/**
	 * 查询科室信息
	 * @param depart
	 * @return
	 */
	public List<DepartmentEntity> retrieveDepartment(@Param("id")String id,@Param("orgId")String orgId,@Param("orgName")String orgName,@Param("orgCode")String orgCode,@Param("deptName")String deptName,
			@Param("parentCode") String parentCode,@Param("currentPage")int currentPage,@Param("rows")int rows);
	
	public List<DepartmentEntity> retrieveDepartments(@Param("orgId")String orgId,@Param("orgCode")String orgCode,@Param("deptName")String deptName,
			@Param("currentPage")int currentPage,@Param("rows")int rows);
	/**
	 * 查询科室信息数量
	 * @param depart
	 * @return
	 */
	public int retrieveDepartmentCount(@Param("id")String id,@Param("orgId")String orgId,@Param("orgName")String orgName,@Param("orgCode")String orgCode,@Param("deptName")String deptName,@Param("parentCode") String parentCode);
	
	public int retrieveDepartmentsCount(@Param("orgId")String orgId,@Param("orgCode")String orgCode,@Param("deptName")String deptName);
	
	
	/**
	 * 通过科室编码查询科室信息（单条）
	 * @param deptCode 科室编码
	 * @return 标识带数据
	 */
	public List<DepartmentEntity> retrieveDepartmentByDeptCode(String deptCode);
	
	/**
	 * 通过机构编码查询科室信息（查询本机构下的所有科室）
	 * @param orgCode
	 * @return
	 */
	public List<DepartmentEntity> retrieveDepartmentByOrgCode(String orgCode);
	
	/**
	 * 依据科室编码删除科室信息
	 * @param deptCode
	 * @return
	 */
	public void deleteDepartmentByDeptCode(String deptCode);
	
	/**
	 * 通过机构编码删除科室信息（删除本机构下所有的科室信息）
	 * @param orgCode
	 * @return
	 */
	public void deleteDepartmentByOrgCode(String orgCode);
	
	/**
	 * 依据机构编码修改科室状态
	 * @param orgCode 机构编码
	 * @param status 状态值
	 */
	public void updateDepartmentByOrgCode(String orgCode, String status);
	
	/**
	 * 依据科室编码修改科室状态
	 * @param deptCode
	 * @param status
	 */
	public void updateDepartmentByDeptCode(String deptCode, String status);
	
	/**
	 * 根据机构编码查找院区
	 * @param orgCode
	 * @return
	 */
	public List<DistrictEntity> getOrgNameByOrgCode(String orgCode);
	
	/**
	 * 通过id删除科室信息
	 * @param id
	 * @return
	 */
	public void deleteDepartmentById(String id);
	
	/**
	 * 根据机构编码、院区标识、科室编码查询科室信息
	 * @param deptCode
	 * @return
	 */
	public DepartmentEntity getDeptByDeptCode(@Param("orgCode")String orgCode,@Param("deptCode")String deptCode);
	
	public void updateParentDept(DepartmentEntity depart);
	
	public List<DeptDto> retrieveAllDept();
	
	public List<DepartmentEntity> retrieveDeptByParentCode(@Param("orgId")String orgId,@Param("parentCode")String parentCode);
	
    public List<DepartmentEntity> retrieveDepartByDeptName(@Param("orgId")String orgId,@Param("orgCode")String orgCode,@Param("deptName")String deptName);


}
