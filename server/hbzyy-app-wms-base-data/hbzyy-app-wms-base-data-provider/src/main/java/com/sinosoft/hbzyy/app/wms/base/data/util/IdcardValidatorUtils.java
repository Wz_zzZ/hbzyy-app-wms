package com.sinosoft.hbzyy.app.wms.base.data.util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;
/**
 * 
*  身份证信息工具
* @author 张鸿阳
* @date 2018年2月26日 下午4:33:49 
* @company:中科软科技股份有限公司
 */
public class IdcardValidatorUtils {
	/**
	 * 省，直辖市代码表： { 11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",
	 * 21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",
	 * 33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",
	 * 42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",
	 * 51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",
	 * 63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"}
	 */
	protected String[][] codeAndCity = { { "11", "北京" }, { "12", "天津" },
			{ "13", "河北" }, { "14", "山西" }, { "15", "内蒙古" }, { "21", "辽宁" },
			{ "22", "吉林" }, { "23", "黑龙江" }, { "31", "上海" }, { "32", "江苏" },
			{ "33", "浙江" }, { "34", "安徽" }, { "35", "福建" }, { "36", "江西" },
			{ "37", "山东" }, { "41", "河南" }, { "42", "湖北" }, { "43", "湖南" },
			{ "44", "广东" }, { "45", "广西" }, { "46", "海南" }, { "50", "重庆" },
			{ "51", "四川" }, { "52", "贵州" }, { "53", "云南" }, { "54", "西藏" },
			{ "61", "陕西" }, { "62", "甘肃" }, { "63", "青海" }, { "64", "宁夏" },
			{ "65", "新疆" }, { "71", "台湾" }, { "81", "香港" }, { "82", "澳门" },
			{ "91", "国外" } };

	public static String[] cityCode = { "11", "12", "13", "14", "15", "21", "22",
			"23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43",
			"44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63",
			"64", "65", "71", "81", "82", "91" };

	/**
	 *  每位加权因子
	 */
	public static int power[] = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };

	/**
	 * 第18位校检码
	 */
	public static String[] verifyCode = { "1", "0", "X", "9", "8", "7", "6", "5",
			"4", "3", "2" };
	/**
	 * 身份证位数 15位
	 */
	private static int CARDNO_15 = 15; 
	/**
	 * 身份证位数 18位
	 */
	private static int CARDNO_18 = 18; 
	/**
	 *  身份证性别权数 2
	 */
	private static int CARD_GENDER = 2;
	
	/**
	 * 验证所有的身份证的合法性
	 * 
	 * @param idcard
	 * @return
	 */
	public static boolean isValidatedAllIdcard(String idcard) {
		if (idcard.length() == CARDNO_15) {
			idcard = convertIdcarBy15bit(idcard);
		}
		return isValidate18Idcard(idcard);
	}

	/**
	 * <p>
	 * 判断18位身份证的合法性
	 * @param idcard
	 * @return
	 */
	public static boolean isValidate18Idcard(String idcard) {
		// 非18位为假
		if (idcard.length() != CARDNO_18) {
			return false;
		}
		// 获取前17位
		String idcard17 = idcard.substring(0, 17);
		// 获取第18位
		String idcard18Code = idcard.substring(17, 18);
		char c[] = null;
		String checkCode = "";
		// 是否都为数字
		if (isDigital(idcard17)) {
			c = idcard17.toCharArray();
		} else {
			return false;
		}

		if (null != c) {
			int bit[] = new int[idcard17.length()];

			bit = converCharToInt(c);

			int sum17 = 0;

			sum17 = getPowerSum(bit);

			// 将和值与11取模得到余数进行校验码判断
			checkCode = getCheckCodeBySum(sum17);
			if (null == checkCode) {
				return false;
			}
			// 将身份证的第18位与算出来的校码进行匹配，不相等就为假
			if (!idcard18Code.equalsIgnoreCase(checkCode)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 将15位的身份证转成18位身份证
	 * 
	 * @param idcard
	 * @return
	 */
	public static String convertIdcarBy15bit(String idcard) {
		String idcard17 = null;
		// 非15位身份证
		if (idcard.length() != CARDNO_15) {
			return null;
		}

		if (isDigital(idcard)) {
			// 获取出生年月日
			String birthday = idcard.substring(6, 12);
			Date birthdate = null;
			try {
				birthdate = new SimpleDateFormat("yyMMdd").parse(birthday);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar cday = Calendar.getInstance();
			cday.setTime(birthdate);
			String year = String.valueOf(cday.get(Calendar.YEAR));

			idcard17 = idcard.substring(0, 6) + year + idcard.substring(8);

			char c[] = idcard17.toCharArray();
			String checkCode = "";

			if (null != c) {
				int bit[] = new int[idcard17.length()];

				// 将字符数组转为整型数组
				bit = converCharToInt(c);
				int sum17 = 0;
				sum17 = getPowerSum(bit);

				// 获取和值与11取模得到余数进行校验码
				checkCode = getCheckCodeBySum(sum17);
				// 获取不到校验位
				if (null == checkCode) {
					return null;
				}

				// 将前17位与第18位校验码拼接
				idcard17 += checkCode;
			}
		} else { // 身份证包含数字
			return null;
		}
		return idcard17;
	}

	/**
	 * 15位和18位身份证号码的基本数字和位数验校
	 * 
	 * @param idcard
	 * @return
	 */
	public static boolean isIdcard(String idcard) {
		return idcard == null || "".equals(idcard) ? false : Pattern.matches(
				"(^\\d{15}$)|(\\d{17}(?:\\d|x|X)$)", idcard);
	}

	/**
	 * 15位身份证号码的基本数字和位数验校
	 * 
	 * @param idcard
	 * @return
	 */
	public static boolean is15Idcard(String idcard) {
		return idcard == null || "".equals(idcard) ? false : Pattern.matches(
				"^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$",
				idcard);
	}

	/**
	 * 18位身份证号码的基本数字和位数验校
	 * 
	 * @param idcard
	 * @return
	 */
	public static boolean is18Idcard(String idcard) {
		return Pattern
				.matches(
						"^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([\\d|x|X]{1})$",
						idcard);
	}

	/**
	 * 数字验证
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isDigital(String str) {
		return str == null || "".equals(str) ? false : str.matches("^[0-9]*$");
	}

	/**
	 * 将身份证的每位和对应位的加权因子相乘之后，再得到和值
	 * 
	 * @param bit
	 * @return
	 */
	public static int getPowerSum(int[] bit) {

		int sum = 0;

		if (power.length != bit.length) {
			return sum;
		}

		for (int i = 0; i < bit.length; i++) {
			for (int j = 0; j < power.length; j++) {
				if (i == j) {
					sum = sum + bit[i] * power[j];
				}
			}
		}
		return sum;
	}

	/**
	 * 将和值与11取模得到余数进行校验码判断
	 * 
	 * @param checkCode
	 * @param sum17
	 * @return 校验位
	 */
	public static String getCheckCodeBySum(int sum17) {
		String checkCode = null;
		switch (sum17 % 11) {
		case 10:
			checkCode = "2";
			break;
		case 9:
			checkCode = "3";
			break;
		case 8:
			checkCode = "4";
			break;
		case 7:
			checkCode = "5";
			break;
		case 6:
			checkCode = "6";
			break;
		case 5:
			checkCode = "7";
			break;
		case 4:
			checkCode = "8";
			break;
		case 3:
			checkCode = "9";
			break;
		case 2:
			checkCode = "x";
			break;
		case 1:
			checkCode = "0";
			break;
		case 0:
			checkCode = "1";
			break;
		default: return "" ;
		}
		return checkCode;
	}

	/**
	 * 将字符数组转为整型数组
	 * 
	 * @param c
	 * @return
	 * @throws NumberFormatException
	 */
	public static int[] converCharToInt(char[] c) throws NumberFormatException {
		int[] a = new int[c.length];
		int k = 0;
		for (char temp : c) {
			a[k++] = Integer.parseInt(String.valueOf(temp));
		}
		return a;
	}
	
	/**
	 * 获取性别 
	 * @param idCardNo 身份证号
	 * @return
	 */
	public static String getGender(String idCardNo){
		//如果是15为身份证 转换为18位身份证
		if(idCardNo.length()== CARDNO_15){
			idCardNo = convertIdcarBy15bit(idCardNo);
		}
		  // 获取性别  
	 	String sGender = "未知的性别";
	 	String id17 = idCardNo.substring(16, 17);  
        if (Integer.parseInt(id17) % CARD_GENDER != 0) {  
        	sGender = "男性";
        } else {  
        	sGender = "女性"; 
        } 
		return sGender;
	}
	
	/**
	 * 根据身份证获取出生日期
	 * @param idCardNo 身份证号
	 * @return 出生日期 时间格式（Date）
	 */
	public static Date getBirthdate(String idCardNo){
		//如果是15为身份证 转换为18位身份证
		if(idCardNo.length()==CARDNO_15){
			idCardNo = convertIdcarBy15bit(idCardNo);
		}
		//获取出生日期
		String birthday = idCardNo.substring(6, 14);  
		Date birthdate = null;
        try {
			birthdate = new SimpleDateFormat("yyyyMMdd")  
		        .parse(birthday);
		} catch (ParseException e) {
			e.printStackTrace();
		}  
		return birthdate;
	}
	
	/**
	 * 根据身份证号获取年龄
	 * @param idCardNo 身份证号
	 * @return 年龄
	 */
	public static String getAge(String idCardNo){
		GregorianCalendar currentDay = new GregorianCalendar();
        int year = currentDay.get(Calendar.YEAR);  
        int month = currentDay.get(Calendar.MONTH) + 1;  
        int day = currentDay.get(Calendar.DAY_OF_MONTH); 
		
		 String years =  idCardNo.substring(6, 10);
         int age = year - Integer.valueOf(years);
         if(age<1){
      	   String months = idCardNo.substring(10, 12);
      	   int ageMonth = month - Integer.valueOf(months);
      	   if(ageMonth==0){
      		   String days = idCardNo.substring(12, 14);
          	   int ageDay = day - Integer.valueOf(days);
          	   return String.valueOf(ageDay)+"天";
      	   } else{
      		 return String.valueOf(ageMonth)+"月";
      	   }
         }else{
        	 return String.valueOf(age)+"岁";
         }
	}
	
	/**
	 * 根据生日计算年龄
	 * @param birthday 生日
	 * @return
	 */
	public static String getAgeByBirthday(String birthday){
		GregorianCalendar currentDay = new GregorianCalendar();
        int year = currentDay.get(Calendar.YEAR);  
        int month = currentDay.get(Calendar.MONTH) + 1;  
        int day = currentDay.get(Calendar.DAY_OF_MONTH);
        if(birthday.contains("-")){        	
        	birthday=birthday.replaceAll("-", "");
        }
        if(birthday.contains("/")){
        	birthday=birthday.replaceAll("/", "");
        }
        int age = year - Integer.valueOf(birthday.substring(0,4));
        if(age<1){
     	   String months = birthday.substring(4, 6);
     	   int ageMonth = month - Integer.valueOf(months);
     	   if(ageMonth==0){
     		   String days = birthday.substring(6,8);
         	   int ageDay = day - Integer.valueOf(days);
         	   return String.valueOf(ageDay)+"天";
     	   } else{
     		 return String.valueOf(ageMonth)+"月";
     	   }
        }else{
       	 return String.valueOf(age)+"岁";
        }
	}
	
	public static void main(String[] args) {
		String idCard = "420111194211243119";
		Date age = getBirthdate(idCard);
		System.out.println(age);
	}
}
