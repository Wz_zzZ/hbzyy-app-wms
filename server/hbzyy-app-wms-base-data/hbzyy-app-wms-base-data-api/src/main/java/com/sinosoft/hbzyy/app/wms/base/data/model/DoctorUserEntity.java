package com.sinosoft.hbzyy.app.wms.base.data.model;

import java.io.Serializable;
import java.util.List;

import com.sinosoft.hbzyy.framework.core.api.model.BaseVO;

public class DoctorUserEntity extends BaseVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 主键id
	 */
	private String id;
	
	/**
	 * 人员姓名
	 */
	private String name;
	
	/**
	 * 人员年龄
	 */
	private String age;
	
	/**
	 * 人员性别
	 */
	private String gender;
	
	/**
	 * 机构编码
	 */
	private String orgCode;
	
	/**
	 * 证件号码
	 */
	private String idCard;
	
	/**
	 * 手机号
	 */
	private String telephone;
	
	/**
	 * 姓名拼音
	 */
	private String namePy;
	
	/**
	 * 机构名称
	 */
	private String orgName;
	
	/**
	 * 院区
	 */
	private String schoolDistrict;
	
	/**
	 * 科室编码
	 */
	private String deptCode;
	
	/**
	 * 科室名称
	 */
	private String deptName;
	
	/**
	 * 职称编码
	 */
	private String title;
	
	/**
	 * 职称编码名称
	 */
	private String titleName;
	
	/**
	 * 个人简介
	 */
	private String resume;
	
	/**
	 * 擅长领域
	 */
	private String field;
	
	/**
	 * 是否启用状态 0 禁用 1 启用
	 */
	private String status;
	
	/**
	 * 头像上传路径
	 */
	private String imageUrl;
	
	/**
	 * 是否开启图文咨询
	 */
	private String isImageText;
	
	/**
	 * 是否开启视频会诊
	 */
	private String isVideo;
	
	/**
	 *网易云accid
	 */
	private String accid;
	
	private String doctorCode;
	
	
	
	public String getDoctorCode() {
		return doctorCode;
	}

	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}

	private String tel;
	
	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	private  List<DepartmentEntity> departs;
	
	
	
	public List<DepartmentEntity> getDeparts() {
		return departs;
	}

	public void setDeparts(List<DepartmentEntity> departs) {
		this.departs = departs;
	}

	public String getAccid() {
		return accid;
	}

	public void setAccid(String accid) {
		this.accid = accid;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getNamePy() {
		return namePy;
	}

	public void setNamePy(String namePy) {
		this.namePy = namePy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getSchoolDistrict() {
		return schoolDistrict;
	}

	public void setSchoolDistrict(String schoolDistrict) {
		this.schoolDistrict = schoolDistrict;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getIsImageText() {
		return isImageText;
	}

	public void setIsImageText(String isImageText) {
		this.isImageText = isImageText;
	}

	public String getIsVideo() {
		return isVideo;
	}

	public void setIsVideo(String isVideo) {
		this.isVideo = isVideo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deptCode == null) ? 0 : deptCode.hashCode());
		result = prime * result + ((deptName == null) ? 0 : deptName.hashCode());
		result = prime * result + ((field == null) ? 0 : field.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imageUrl == null) ? 0 : imageUrl.hashCode());
		result = prime * result + ((isImageText == null) ? 0 : isImageText.hashCode());
		result = prime * result + ((isVideo == null) ? 0 : isVideo.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((orgCode == null) ? 0 : orgCode.hashCode());
		result = prime * result + ((orgName == null) ? 0 : orgName.hashCode());
		result = prime * result + ((resume == null) ? 0 : resume.hashCode());
		result = prime * result + ((schoolDistrict == null) ? 0 : schoolDistrict.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((titleName == null) ? 0 : titleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DoctorUserEntity other = (DoctorUserEntity) obj;
		if (deptCode == null) {
			if (other.deptCode != null)
				return false;
		} else if (!deptCode.equals(other.deptCode))
			return false;
		if (deptName == null) {
			if (other.deptName != null)
				return false;
		} else if (!deptName.equals(other.deptName))
			return false;
		if (field == null) {
			if (other.field != null)
				return false;
		} else if (!field.equals(other.field))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageUrl == null) {
			if (other.imageUrl != null)
				return false;
		} else if (!imageUrl.equals(other.imageUrl))
			return false;
		if (isImageText == null) {
			if (other.isImageText != null)
				return false;
		} else if (!isImageText.equals(other.isImageText))
			return false;
		if (isVideo == null) {
			if (other.isVideo != null)
				return false;
		} else if (!isVideo.equals(other.isVideo))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (orgCode == null) {
			if (other.orgCode != null)
				return false;
		} else if (!orgCode.equals(other.orgCode))
			return false;
		if (orgName == null) {
			if (other.orgName != null)
				return false;
		} else if (!orgName.equals(other.orgName))
			return false;
		if (resume == null) {
			if (other.resume != null)
				return false;
		} else if (!resume.equals(other.resume))
			return false;
		if (schoolDistrict == null) {
			if (other.schoolDistrict != null)
				return false;
		} else if (!schoolDistrict.equals(other.schoolDistrict))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (titleName == null) {
			if (other.titleName != null)
				return false;
		} else if (!titleName.equals(other.titleName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DoctorUserEntity [id=" + id + ", name=" + name + ", gender=" + gender + ", orgCode=" + orgCode
				+ ", idCard=" + idCard + ", namePy=" + namePy + ", orgName=" + orgName + ", schoolDistrict="
				+ schoolDistrict + ", deptCode=" + deptCode + ", deptName=" + deptName + ", title=" + title
				+ ", titleName=" + titleName + ", resume=" + resume + ", field=" + field + ", status=" + status
				+ ", imageUrl=" + imageUrl + ", isImageText=" + isImageText + ", isVideo=" + isVideo + "]";
	}

}
