package com.sinosoft.hbzyy.app.wms.base.data.model;

import com.sinosoft.hbzyy.framework.core.api.model.BaseVO;
/**
 * 
 * @author zhaofei
 * @description:科室大类实体类
 * @date：2018年4月27日
 * @type：ParentDeptEntity
 * @version: v1.0.0
 * @company:中科软
 */
public class ParentDeptEntity extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 科室大类的主键id
	 */
	private String id;
	
	/**
	 * 科室大类编号
	 */
	private String code;
	
	/**
	 * 科室大类名称
	 */
	private String name;
	
	/**
	 * 机构id
	 */
	private String orgId;
	
	/**
	 * 机构名称
	 */
	private String orgName;
	
	/**
	 * 备注信息
	 */
	private String remark;
	
	/**
	 * 状态
	 */
	private String status;
	
	/**
	 * 修改时间
	 */
	private Long updateAt;
	
	/**
	 * 更新时间
	 */
	private Long createAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Long updateAt) {
		this.updateAt = updateAt;
	}

	public Long getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Long createAt) {
		this.createAt = createAt;
	}

	@Override
	public String toString() {
		return "ParentDept [id=" + id + ", code=" + code + ", name=" + name + ", orgId=" + orgId + ", orgName="
				+ orgName + ", remark=" + remark + ", status=" + status + ", updateAt=" + updateAt + ", createAt="
				+ createAt + "]";
	}


	
	
	
	

}
