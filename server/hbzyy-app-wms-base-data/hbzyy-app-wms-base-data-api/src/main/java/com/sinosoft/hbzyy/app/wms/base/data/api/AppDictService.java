package com.sinosoft.hbzyy.app.wms.base.data.api;

import java.util.List;

import com.sinosoft.hbzyy.app.wms.base.data.model.AppDictItemEntity;

public interface AppDictService {
	
	/**
	 * 通过父Id查询代码明细
	 * @return
	 */
	public List<AppDictItemEntity> retrieveItemByDid(String did);
	
	/**
	 * 通过父编码查询行政区划代码
	 * @param pcode
	 * @return
	 */
	public List<AppDictItemEntity> retrieveItemByPcode(String pcode);

}
