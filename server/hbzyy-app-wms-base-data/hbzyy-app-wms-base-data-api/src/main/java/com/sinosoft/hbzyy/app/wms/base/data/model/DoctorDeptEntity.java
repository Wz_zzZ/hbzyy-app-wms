package com.sinosoft.hbzyy.app.wms.base.data.model;

import java.io.Serializable;
import java.util.Date;

import com.sinosoft.hbzyy.framework.core.api.model.BaseVO;


public class DoctorDeptEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
	 * 用户（医生ID）
	 */
	private String docId;
	/**
	 * 机构编码
	 */
	private String orgCode;
	/**
	 * 科室编码
	 */
	private String deptCode;
	
	/**
	 * 状态值
	 */
	private String status;
	
	private String districtId;


	public String getDistrictId() {
		return districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deptCode == null) ? 0 : deptCode.hashCode());
		result = prime * result + ((docId == null) ? 0 : docId.hashCode());
		result = prime * result + ((orgCode == null) ? 0 : orgCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DoctorDeptEntity other = (DoctorDeptEntity) obj;
		if (deptCode == null) {
			if (other.deptCode != null)
				return false;
		} else if (!deptCode.equals(other.deptCode))
			return false;
		if (docId == null) {
			if (other.docId != null)
				return false;
		} else if (!docId.equals(other.docId))
			return false;
		if (orgCode == null) {
			if (other.orgCode != null)
				return false;
		} else if (!orgCode.equals(other.orgCode)) {
			return false;
		}	
		return true;
	}

	@Override
	public String toString() {
		return "DoctorDeptEntity [docId=" + docId + ", orgCode=" + orgCode + ", deptCode="
				+ deptCode +  "]";
	}
	
	
	
}
