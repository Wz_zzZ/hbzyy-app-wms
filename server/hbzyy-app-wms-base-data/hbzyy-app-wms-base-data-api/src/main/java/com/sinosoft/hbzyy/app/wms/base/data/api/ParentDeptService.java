package com.sinosoft.hbzyy.app.wms.base.data.api;

import java.util.Map;

import com.sinosoft.hbzyy.app.wms.base.data.model.ParentDeptEntity;
/**
 * 科室大类接口实现类
 * @author zhaofei
 *
 */
public interface ParentDeptService {

	/**
	 * 新增大科室分类
	 * @param parentDept
	 * @return
	 */
	public Map<String,Object> createParentDept(ParentDeptEntity parentDept);
	
	/**
	 * 更新大科室信息
	 * @param parentDept
	 * @return
	 */
	public Map<String,Object> updateParentDept(ParentDeptEntity parentDept);
	
	/**
	 * 查询科室科室大类
	 * @param orgCode
	 * @param code
	 * @param status
	 * @return
	 */
	public Map<String,Object> retrieveParentDept(String orgCode,String code,String status);
	
	/**
	 * 删除科室大类
	 * @param id
	 * @return
	 */
	public Map<String,Object> deleteParentDept(String id);
	
}
