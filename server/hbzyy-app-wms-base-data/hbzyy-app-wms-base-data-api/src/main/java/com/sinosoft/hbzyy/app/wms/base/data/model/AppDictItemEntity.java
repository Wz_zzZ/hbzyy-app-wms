package com.sinosoft.hbzyy.app.wms.base.data.model;

import java.io.Serializable;

/**
 * 
 * @author 刘富鹏
 * @description:字典表明细
 * @date：2018年3月27日下午1:42:19
 * @type：AppDictItemEntity
 * @version: 
 * @company:
 */
public class AppDictItemEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2471807706262280750L;

	/**
	 * 主键
	 */
	private String id;
	
	/**
	 * 父id
	 */
	private String did;
	
	/**
	 * 编码
	 */
	private String code;
	
	/**
	 * 值
	 */
	private String value;
	
	/**
	 * 父编码
	 */
	private String pcode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDid() {
		return did;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((did == null) ? 0 : did.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((pcode == null) ? 0 : pcode.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppDictItemEntity other = (AppDictItemEntity) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (did == null) {
			if (other.did != null)
				return false;
		} else if (!did.equals(other.did))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (pcode == null) {
			if (other.pcode != null)
				return false;
		} else if (!pcode.equals(other.pcode))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AppDictItemEntity [id=" + id + ", did=" + did + ", code=" + code + ", value=" + value + ", pcode="
				+ pcode + "]";
	}
}
