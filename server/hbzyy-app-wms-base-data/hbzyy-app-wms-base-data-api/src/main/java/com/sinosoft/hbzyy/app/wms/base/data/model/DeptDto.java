package com.sinosoft.hbzyy.app.wms.base.data.model;

import java.io.Serializable;
import java.util.List;

public class DeptDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private ParentDeptEntity parentDept;
	
	private List<DepartmentEntity> deptList;

	public ParentDeptEntity getParentDept() {
		return parentDept;
	}

	public void setParentDept(ParentDeptEntity parentDept) {
		this.parentDept = parentDept;
	}

	public List<DepartmentEntity> getDeptList() {
		return deptList;
	}

	public void setDeptList(List<DepartmentEntity> deptList) {
		this.deptList = deptList;
	}

	@Override
	public String toString() {
		return "DeptDto [parentDept=" + parentDept + ", deptList=" + deptList + "]";
	}


	

}
