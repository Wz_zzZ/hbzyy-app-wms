package com.sinosoft.hbzyy.app.wms.base.data.api;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;

import com.sinosoft.hbzyy.app.wms.base.data.model.DepartmentEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;

/**
 * 
 * @author 刘富鹏
 * @description:科室信息服务层
 * @date：2018年3月20日下午4:12:50
 * @type：DepartmentService
 * @version: 1.0
 * @company:中科软
 */
public interface DepartmentService {
	
	/**
	 * 创建科室信息
	 * @param depart 科室信息
	 * @return 标识带数据
	 * @throws IOException, RuntimeException 
	 * @throws JsonParseException 
	 */
	public Map<String,Object> createDepartment(List<DepartmentEntity> departs) throws JsonParseException, IOException, RuntimeException;
	
	/**
	 * 更新科室信息
	 * @param depart 科室信息
	 * @return 标识带数据
	 */
	public Map<String,Object> updateDepartment(DepartmentEntity depart);
	
	/**
	 * 查询科室信息
	 * @param depart
	 * @return
	 */
	public List<DepartmentEntity> retrieveDepartment(String id,String orgId,String orgName,String orgCode,String deptName,String parentCode,int currentPage,int rows);
	
	/**
	 * 通过科室编码查询科室信息（单条）
	 * @param deptCode 科室编码
	 * @return 标识带数据
	 */
	//1
	public Map<String,Object> retrieveDepartments(String orgId,String orgCode,String deptName,int currentPage,int rows);
	
	public Map<String,Object> retrieveDepartmentByDeptCode(String deptCode);
	
	/**
	 * 通过机构编码查询科室信息（查询本机构下的所有科室）
	 * @param orgCode
	 * @return
	 */
	public Map<String,Object> retrieveDepartmentByOrgCode(String orgCode);
	
	/**
	 * 依据科室编码删除科室信息
	 * @param deptCode
	 * @return
	 */
	public Map<String,Object> updateDepartmentByDeptCode(String deptCode,String status);
	
	/**
	 * 通过机构编码删除科室信息（删除本机构下所有的科室信息）
	 * @param orgCode
	 * @return
	 */
	public Map<String,Object> deleteDepartmentByOrgCode(String orgCode);
	
	/**
	 * 通过id删除科室信息（删除本机构下所有的科室信息）
	 * @param id
	 * @return
	 */
	public Map<String,Object> deleteDepartmentById(String id);
	
	/**
	 * 专门为科室管理功能查找机构下面的院区
	 * @param orgCode
	 * @return
	 */
	public Map<String,Object> getOrgNameByOrgCode(String orgCode);
	
	public Map<String,Object> retrieveAllDept(String orgCode,String orgId,String deptName);
	
	public Map<String,Object> retrieveDepart(String orgCode,String orgId,String deptName);
	
}
