package com.sinosoft.hbzyy.app.wms.base.data.model;

import java.io.Serializable;
import java.util.List;

import com.sinosoft.hbzyy.framework.core.api.model.BaseVO;

public class OrganizationEntity extends BaseVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 主键id
	 */
	private String id;
	
	/**
	 * 机构名称
	 */
	private String orgName;
	
	/**
	 * 机构编码拼音
	 */
	private String orgNamePy;
	
	/**
	 * 机构编码
	 */
	private String orgCode;
	
	/**
	 * 机构简称
	 */
	private String shortName;
	
	/**
	 * 等级
	 */
	private String level;
	
	/**
	 * 等级名称
	 */
	private String levelName;
	
	/**
	 * 区域编码
	 */
	private String areaCode;
	
	/**
	 * 区域编码名称
	 */
	private String areaName;
	
	/**
	 * 机构法人代表
	 */
	private String legalPerson;
	
	/**
	 * 机构地址
	 */
	private String address;
	
	/**
	 * 机构联系人
	 */
	private String contactPerson;
	
	/**
	 * 机构联系电话
	 */
	private String contactPhone;
	
	/**
	 * 邮政编码
	 */
	private String orgPostCode;
	
	/**
	 * 开户人
	 */
	private String openPerson;
	
	/**
	 * 开户银行
	 */
	private String openBank;
	
	/**
	 * 银行账号
	 */
	private String bankNo;
	
	/**
	 * 开户网点
	 */
	private String openWeb;
	
	/**
	 * 支付宝账号
	 */
	private String alipayNo;
	
	/**
	 * 机构状态值 停用或禁用
	 */
	private String status;
	
	/**
	 * 院区
	 */
	private String schoolDistrict;
	
	/**
	 *院区名称
	 */
	private String schoolDistrictName;
	
	/**
	 * 营业执照存放路径
	 */
	private String busPhotPath;
	
	/**
	 * 医院照片存放路径
	 */
	private String hosPhotPath;

	private List<DistrictEntity> schoolDis;
	
	private List<ParentDeptEntity> parentDept;
	
	
	public List<ParentDeptEntity> getParentDept() {
		return parentDept;
	}

	public void setParentDept(List<ParentDeptEntity> parentDept) {
		this.parentDept = parentDept;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public List<DistrictEntity> getSchoolDis() {
		return schoolDis;
	}

	public void setSchoolDis(List<DistrictEntity> schoolDis) {
		this.schoolDis = schoolDis;
	}

	public String getSchoolDistrictName() {
		return schoolDistrictName;
	}

	public void setSchoolDistrictName(String schoolDistrictName) {
		this.schoolDistrictName = schoolDistrictName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getLegalPerson() {
		return legalPerson;
	}

	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getOrgPostCode() {
		return orgPostCode;
	}

	public void setOrgPostCode(String orgPostCode) {
		this.orgPostCode = orgPostCode;
	}

	public String getOpenPerson() {
		return openPerson;
	}

	public void setOpenPerson(String openPerson) {
		this.openPerson = openPerson;
	}

	public String getOpenBank() {
		return openBank;
	}

	public void setOpenBank(String openBank) {
		this.openBank = openBank;
	}

	public String getBankNo() {
		return bankNo;
	}

	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}

	public String getOpenWeb() {
		return openWeb;
	}

	public void setOpenWeb(String openWeb) {
		this.openWeb = openWeb;
	}

	public String getAlipayNo() {
		return alipayNo;
	}

	public void setAlipayNo(String alipayNo) {
		this.alipayNo = alipayNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSchoolDistrict() {
		return schoolDistrict;
	}

	public void setSchoolDistrict(String schoolDistrict) {
		this.schoolDistrict = schoolDistrict;
	}

	public String getBusPhotPath() {
		return busPhotPath;
	}

	public void setBusPhotPath(String busPhotPath) {
		this.busPhotPath = busPhotPath;
	}

	public String getHosPhotPath() {
		return hosPhotPath;
	}

	public void setHosPhotPath(String hosPhotPath) {
		this.hosPhotPath = hosPhotPath;
	}
	
	

	public String getOrgNamePy() {
		return orgNamePy;
	}

	public void setOrgNamePy(String orgNamePy) {
		this.orgNamePy = orgNamePy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((alipayNo == null) ? 0 : alipayNo.hashCode());
		result = prime * result + ((areaCode == null) ? 0 : areaCode.hashCode());
		result = prime * result + ((areaName == null) ? 0 : areaName.hashCode());
		result = prime * result + ((bankNo == null) ? 0 : bankNo.hashCode());
		result = prime * result + ((busPhotPath == null) ? 0 : busPhotPath.hashCode());
		result = prime * result + ((contactPerson == null) ? 0 : contactPerson.hashCode());
		result = prime * result + ((contactPhone == null) ? 0 : contactPhone.hashCode());
		result = prime * result + ((hosPhotPath == null) ? 0 : hosPhotPath.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((legalPerson == null) ? 0 : legalPerson.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((openBank == null) ? 0 : openBank.hashCode());
		result = prime * result + ((openPerson == null) ? 0 : openPerson.hashCode());
		result = prime * result + ((openWeb == null) ? 0 : openWeb.hashCode());
		result = prime * result + ((orgCode == null) ? 0 : orgCode.hashCode());
		result = prime * result + ((orgName == null) ? 0 : orgName.hashCode());
		result = prime * result + ((orgPostCode == null) ? 0 : orgPostCode.hashCode());
		result = prime * result + ((schoolDistrict == null) ? 0 : schoolDistrict.hashCode());
		result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationEntity other = (OrganizationEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (alipayNo == null) {
			if (other.alipayNo != null)
				return false;
		} else if (!alipayNo.equals(other.alipayNo))
			return false;
		if (areaCode == null) {
			if (other.areaCode != null)
				return false;
		} else if (!areaCode.equals(other.areaCode))
			return false;
		if (areaName == null) {
			if (other.areaName != null)
				return false;
		} else if (!areaName.equals(other.areaName))
			return false;
		if (bankNo == null) {
			if (other.bankNo != null)
				return false;
		} else if (!bankNo.equals(other.bankNo))
			return false;
		if (busPhotPath == null) {
			if (other.busPhotPath != null)
				return false;
		} else if (!busPhotPath.equals(other.busPhotPath))
			return false;
		if (contactPerson == null) {
			if (other.contactPerson != null)
				return false;
		} else if (!contactPerson.equals(other.contactPerson))
			return false;
		if (contactPhone == null) {
			if (other.contactPhone != null)
				return false;
		} else if (!contactPhone.equals(other.contactPhone))
			return false;
		if (hosPhotPath == null) {
			if (other.hosPhotPath != null)
				return false;
		} else if (!hosPhotPath.equals(other.hosPhotPath))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (legalPerson == null) {
			if (other.legalPerson != null)
				return false;
		} else if (!legalPerson.equals(other.legalPerson))
			return false;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		if (openBank == null) {
			if (other.openBank != null)
				return false;
		} else if (!openBank.equals(other.openBank))
			return false;
		if (openPerson == null) {
			if (other.openPerson != null)
				return false;
		} else if (!openPerson.equals(other.openPerson))
			return false;
		if (openWeb == null) {
			if (other.openWeb != null)
				return false;
		} else if (!openWeb.equals(other.openWeb))
			return false;
		if (orgCode == null) {
			if (other.orgCode != null)
				return false;
		} else if (!orgCode.equals(other.orgCode))
			return false;
		if (orgName == null) {
			if (other.orgName != null)
				return false;
		} else if (!orgName.equals(other.orgName))
			return false;
		if (orgPostCode == null) {
			if (other.orgPostCode != null)
				return false;
		} else if (!orgPostCode.equals(other.orgPostCode))
			return false;
		if (schoolDistrict == null) {
			if (other.schoolDistrict != null)
				return false;
		} else if (!schoolDistrict.equals(other.schoolDistrict))
			return false;
		if (shortName == null) {
			if (other.shortName != null)
				return false;
		} else if (!shortName.equals(other.shortName))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrganizationEntity [id=" + id + ", orgName=" + orgName + ", orgCode=" + orgCode + ", shortName="
				+ shortName + ", level=" + level + ", areaCode=" + areaCode + ", areaName=" + areaName
				+ ", legalPerson=" + legalPerson + ", address=" + address + ", contactPerson=" + contactPerson
				+ ", contactPhone=" + contactPhone + ", orgPostCode=" + orgPostCode + ", openPerson=" + openPerson
				+ ", openBank=" + openBank + ", bankNo=" + bankNo + ", openWeb=" + openWeb + ", alipayNo=" + alipayNo
				+ ", status=" + status + ", schoolDistrict=" + schoolDistrict + ", busPhotPath=" + busPhotPath
				+ ", hosPhotPath=" + hosPhotPath + "]";
	}

}
