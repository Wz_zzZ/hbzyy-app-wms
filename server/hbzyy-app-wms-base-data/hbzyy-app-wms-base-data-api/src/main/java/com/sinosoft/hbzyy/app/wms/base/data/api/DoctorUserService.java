package com.sinosoft.hbzyy.app.wms.base.data.api;

import java.util.Map;

import com.sinosoft.hbzyy.app.wms.base.data.model.DoctorUserEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;

/**
 * 
 * @author 刘富鹏
 * @description:人员信息服务
 * @date：2018年3月20日下午4:15:05
 * @type：DoctorUserService
 * @version: 1.0
 * @company:中科软
 */
public interface DoctorUserService {
	
	/**
	 * 创建人员信息
	 * @param doctor 人员信息
	 * @return 标识带数据
	 */
	public Map<String,Object> createDoctorUser(DoctorUserEntity doctor);
	
	/**
	 * 更新人员信息
	 * @param doctor 人员信息
	 * @return 标识带数据
	 */
	public Map<String,Object> updateDoctorUser(DoctorUserEntity doctor);
	
	/**
	 * 更新是否有图文咨询和视频问诊权限
	 * @param id 主键id	
	 * @param isText 是否有图文咨询 0 没有 1 有
	 * @param isVideo 是否有视频会诊权限 0 没有 1有
	 * @return	标识带数据
	 */
	public Map<String,Object> updateDoctorTextAndVideo(String id,String isText,String isVideo);
	
	/**
	 * 更新医生是否启用状态 
	 * @param id 主键id
	 * @param status 状态值 0 禁用 1启用
	 * @return 标识带数据
	 */
	public Map<String,Object> updateDoctorStatusById(String id,String status);
	
	/**
	 * 通过电话号码查询人员信息
	 * @param telephone 手机号码
	 * @return 标识带数据
	 */
	public Map<String,Object> retrieveDoctorByTelephone(String telephone);
	
	/**
	 * 通过姓名查询医生信息
	 * @param name 医生人员姓名
	 * @return 标识带数据
	 */
	public Map<String,Object> retrieveDoctorByName(String name);
	
	/**
	 * 通过姓名查询医生信息
	 * @param name 医生人员姓名
	 * @return 标识带数据
	 */
	public PageList<DoctorUserEntity> retrieveDoctorUser(String id,String orgCode,String name,String deptCode,int currentPage,int rows);
	
	/**
	 * 通过机构编码查询人员信息
	 * @param orgCode 机构编码
	 * @return	标识带数据
	 */
	public Map<String,Object> retrieveDoctorByOrgCode(String orgCode);
	
	/**
	 * 通过机构编码和科室编码查询人员信息
	 * @param orgCode 机构编码
	 * @param deptCode 科室编码
	 * @return 标识带数据
	 */
	public Map<String,Object> retrieveDoctorByOrgCodeAndDeptCode(String orgCode,String deptCode);
	
	/**
	 * 删除人员信息，同时删除人员科室对应信息
	 * @param id
	 */
	public void deleteDoctorUserById(String id);
	
	public Map<String,Object> retrieveDoctorAccidByDoctorCode(String doctorCode);
	
	public String retrieveAccidByDoctorCode(String doctorCode);
	
	public Map<String,Object> retrieveDoctorByPinYin(String namePy,int curpage,int pageSize);
	
	public Map<String,Object> retrieveDoctorAllByName(String name);

}
