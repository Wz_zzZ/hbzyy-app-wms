package com.sinosoft.hbzyy.app.wms.base.data.model;

import java.io.Serializable;

import com.sinosoft.hbzyy.framework.core.api.model.BaseVO;

public class DistrictEntity extends BaseVO implements Serializable {
	private static final long serialVersionUID = -2252715872243892199L;
	
	/**
	 * 主键id
	 */
	private String id;
	
	/**
	 * 院区id
	 */
	private String districtId;
	
	/**
	 * 机构编码
	 */
	private String orgCode;
	
	/**
	 * 院区名称
	 */
	private String districtName;
	
	/**
	 * 机构名称
	 */
	private String orgName;
	
	/**
	 * 地址
	 */
	private String address;
	
	/**
	 * 院区描述信息
	 */
	private String description;
	
	/**
	 * 图片上传路径
	 */
	private String path;
	
	/**
	 * 状态值
	 */
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDistrictId() {
		return districtId;
	}

	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

}
