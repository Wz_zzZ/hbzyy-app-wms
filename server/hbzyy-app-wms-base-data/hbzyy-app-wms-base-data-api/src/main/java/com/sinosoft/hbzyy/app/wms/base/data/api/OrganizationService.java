package com.sinosoft.hbzyy.app.wms.base.data.api;

import java.util.Map;

import com.sinosoft.hbzyy.app.wms.base.data.model.OrganizationEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.DistrictEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;

/**
 * 
 * @author 刘富鹏
 * @description:机构信息服务
 * @date：2018年3月20日下午3:04:16
 * @type：OrganizationService
 * @version:1.0 
 * @company:中科软
 */
public interface OrganizationService {
	
	/**
	 * 创建机构信息服务
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	public Map<String,Object> createOrganization(OrganizationEntity organ);
	
	/**
	 * 创建机构信息和院区信息服务
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	public Map<String,Object> createOrganization(OrganizationEntity organ,DistrictEntity district);
	
	/**
	 * 修改机构信息
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	public Map<String,Object> updateOrganization(OrganizationEntity organ);
	
	/**
	 * 依据机构编码查询机构信息
	 * @param orgCode 机构编码
	 * @return 标识带数据
	 */
	public Map<String,Object> retrieveOrganizationByOrgCode(String orgCode);
	
	/**
	 * 多条件查询机构信息带分页
	 * @param organ 机构信息入参
	 * @return 标识带数据
	 */
	public PageList<OrganizationEntity> retrieveOrganization(String orgName,String orgCode,String level,int currentPage,int rows);
	
	/**
	 * 通过机构编码物理删除机构信息
	 * @param orgCode 机构编码
	 * @return 标识带数据
	 */
	public Map<String,Object> deleteOrganizationByOrgCode(String orgCode);
	
	/**
	 * 通过机构编码更新状态（逻辑删除机构信息）
	 * @param orgCode
	 * @param status
	 * @return
	 */
	public Map<String,Object> updateOrganizationByOrgCode(String orgCode,String status);
	
	/**
	 * 更新院区
	 * @param schools
	 * @return
	 */
	public Map<String,Object> updateDistrict(DistrictEntity district);
	
	/**
	 * 删除院区
	 * @param id
	 * @return
	 */
	public Map<String,Object> deleteDistrictById(String id);
	
	/**
	 * 增加院区信息
	 * @param district
	 * @return
	 */
	public Map<String,Object> createDistrict(DistrictEntity district);
	
	/**
	 * 依据科室编码删除院区信息
	 * @param orgCode
	 */
	public void deleteDistrictByOrgCode(String orgCode);
	
	/**
	 * 依据机构编码和院区id删除院区
	 * @param orgCode
	 * @param districtId
	 */
	public void deleteDistrictByOrgCodeAndDistrictId(String orgCode,String districtId);
	
	/**
	 * 依据机构编码和院区id查询院区信息
	 * @param orgCode
	 * @param districtId
	 * @return
	 */
	public DistrictEntity retrieveDistrictByOrgCodeAndDistictId(String orgCode,String districtId,String districtName);	
	
	public Map<String,Object> retrieveDistrictByOrgCode (String orgCode);
	
	public Map<String,Object> retrieveAllOrg();

}
