package com.sinosoft.hbzyy.app.wms.base.data.endpoint;

import java.io.InputStream;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import com.sinosoft.hbzyy.app.wms.base.data.model.OrganizationEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.DistrictEntity;

public interface OrganizationEndpoint {
	
	
	/**
	 * 创建机构信息服务
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/createOrgan")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> createOrganization(OrganizationEntity organ);
	
	/**
	 * 修改机构信息
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/updateOrgan")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateOrganization(OrganizationEntity organ);
	
	/**
	 * 修改机构信息和院区信息
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/updateOrganAndDistict")
	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateOrganization(@Multipart("organ")String  organ,@Multipart("district")String district);
	
	
	/**
	 * 多条件查询机构信息带分页
	 * @param organ 机构信息入参
	 * @return 标识带数据
	 */
	@GET
	@Path("/retrieveOrgan")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveOrganization(@QueryParam("orgName")String orgName,@QueryParam("orgCode")String orgCode,
			@QueryParam("level")String level,@QueryParam("currentPage")Integer currentPage,@QueryParam("rows")Integer rows);
	
	
	/**
	 * 通过机构编码更新状态（逻辑删除机构信息）
	 * @param orgCode
	 * @param status
	 * @return
	 */
	@POST
	@Path("/updateStatus")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateOrganizationByOrgCode(Map<String,Object> map);
	
	/**
	 * 更新院区
	 * @param schools
	 * @return
	 */
	@POST
	@Path("/updateSchoolDis")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateSchoolDis(DistrictEntity schools);
	
	/**
	 * 删除院区
	 * @param id
	 * @return
	 */
	@GET
	@Path("/deleteSchoolDisById")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> deleteSchoolDisById(@QueryParam("id")String id);
	
	/**
	 * 删除院区
	 * @param id
	 * @return
	 */
	@GET
	@Path("/deleteSchoolDisByOrgCodeAndDistrictId")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> deleteDistrictByOrgCodeAndDistrictId(@QueryParam("orgCode")String orgCode,@QueryParam("districtId")String districtId);
	
	/**
	 * 上传图片信息
	 * @param pictureInfo
	 * @param is
	 * @return
	 */
	@POST
	@Path("/uploadNews")
	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> uploadPicture(@Multipart("file")InputStream is);
	
	/**
	 * 通过父id查询字典信息
	 * @param id
	 * @return
	 */
	@GET
	@Path("/retrieveAppItemByDid")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveAppItemByDid(@QueryParam("did")String did);
	
	/**
	 * 通过父编码查询字典信息
	 * @param id
	 * @return
	 */
	@GET
	@Path("/retrieveAppItemByPcode")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveAppItemByPcode(@QueryParam("pcode")String pcode);
	
	/**
	 * 创建机构信息和院区信息服务
	 * @param organ 机构信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/createOrganAndDist")
	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> createOrganization(@Multipart("organ")String organ,@Multipart("district")String district);
	
	@GET
	@Path("/retrieveDist")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveDist(@QueryParam("orgCode")String orgCode);
	
	@GET
	@Path("/retrieveAllOrg")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveAllOrg();
	
}
