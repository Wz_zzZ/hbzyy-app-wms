package com.sinosoft.hbzyy.app.wms.base.data.endpoint;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sinosoft.hbzyy.app.wms.base.data.model.DepartmentEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.ParentDeptEntity;

public interface DepartmentEndpoint {
	
	/**
	 * 创建科室信息
	 * @param depart 科室信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/createDepart")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> createDepartment(List<DepartmentEntity> departs);
	
	/**
	 * 更新科室信息
	 * @param depart 科室信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/updateDepart")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateDepartment(DepartmentEntity depart);
	
	/**
	 * 查询科室信息
	 * @param depart
	 * @return
	 */
	@GET
	@Path("/retrieveDepart")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveDepartment(@QueryParam("id")String id,@QueryParam("orgId")String orgId,@QueryParam("orgName")String orgName,@QueryParam("orgCode")String orgCode,
			@QueryParam("deptName")String deptName,@QueryParam("parentCode")String parentCode,@QueryParam("currentPage")int currentPage,
			@QueryParam("rows")int rows);
	
	/**
	 * 依据科室编码删除科室信息
	 * @param deptCode
	 * @return
	 */
	@POST
	@Path("/updateStatus")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateDepartmentByDeptCode(String deptCode,String status);
	
	/**
	 * 根据机构编码获取院区名字
	 * @param orgCode
	 * @return
	 */
	@GET
	@Path("/getOrgName")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> getOrgNameByOrgCode(@QueryParam("orgCode")String orgCode);
	
	/**
	 * 根据id删除科室信息
	 * @param id
	 * @return
	 */
	@GET
	@Path("/deleteDept")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> deleteDeptById(@QueryParam("id")String id);
	
	/**
	 * 新增科室大类
	 * @param parentDept
	 * @return
	 */
	@POST
	@Path("/createParentDept")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> createParentDept(ParentDeptEntity parentDept);
	
	/**
	 * 更新科室大类
	 * @param parentDept
	 * @return
	 */
	@POST
	@Path("/updateParentDept")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateParentDept(ParentDeptEntity parentDept);
	
	/**
	 * 查询科室大类
	 * @param orgCode
	 * @param code
	 * @param status
	 * @return
	 */
	@GET
	@Path("/retrieveParentDept")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveParentDept(@QueryParam("orgCode")String orgCode, @QueryParam("code")String code, @QueryParam("status")String status);
	
	/**
	 * 删除科室大类
	 * @param id
	 * @return
	 */
	@GET
	@Path("/deleteParentDept")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> deleteParentDept(@QueryParam("id")String id);
	
	@GET
	@Path("/retrieveDeparts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveDeparts(@QueryParam("orgId")String orgId,@QueryParam("orgCode")String orgCode,@QueryParam("deptName")String deptName,@QueryParam("currentPage")int currentPage,@QueryParam("rows")int rows);
	
	@GET
	@Path("/retrieveAllDeparts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object>retrieveAllDeparts(@QueryParam("orgCode")String orgCode,@QueryParam("orgId")String orgId,@QueryParam("deptName")String deptName);
	
	@GET
	@Path("/retrieveDepartByDeptName")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveDepart(@QueryParam("orgCode")String orgCode,@QueryParam("orgId")String orgId,@QueryParam("deptName")String deptName);
	
}
