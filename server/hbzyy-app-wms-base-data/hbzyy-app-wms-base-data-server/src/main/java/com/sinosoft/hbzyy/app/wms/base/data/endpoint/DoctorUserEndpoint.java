package com.sinosoft.hbzyy.app.wms.base.data.endpoint;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import com.sinosoft.hbzyy.app.wms.base.data.model.DoctorUserEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;

public interface DoctorUserEndpoint {
	
	/**
	 * 创建人员信息
	 * @param doctor 人员信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/createDoct")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> createDoctorUser(DoctorUserEntity doctor);
	
	/**
	 * 更新人员信息
	 * @param doctor 人员信息
	 * @return 标识带数据
	 */
	@POST
	@Path("/updateDoct")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateDoctorUser(DoctorUserEntity doctor);
	
	/**
	 * 更新是否有图文咨询和视频问诊权限
	 * @param id 主键id	
	 * @param isText 是否有图文咨询 0 没有 1 有
	 * @param isVideo 是否有视频会诊权限 0 没有 1有
	 * @return	标识带数据
	 */
	@POST
	@Path("/updateVideoAndText")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateDoctorTextAndVideo(Map<String,Object> map);
	
	
	/**
	 * 通过姓名查询医生信息
	 * @param name 医生人员姓名
	 * @return 标识带数据
	 */
	@GET
	@Path("/retrieveDoct")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveDoctorUser(@QueryParam("id")String id,@QueryParam("orgCode")String orgCode,@QueryParam("name")String name,
			@QueryParam("deptCode")String deptCode,@QueryParam("currentPage")Integer currentPage,
			@QueryParam("rows")Integer rows);
	
	
	/**
	 * 上传图片信息
	 * @param pictureInfo
	 * @param is
	 * @return
	 */
	@POST
	@Path("/uploadDocPicture")
	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> uploadPicture(@Multipart("pictureInfo")String pictureInfo,@Multipart("picture")InputStream is);
	
	@GET
	@Path("/retrieveAccidByCode")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void retrieveAccidByCode(@QueryParam("doctorCode")String doctorCode);
	
	
	@GET
	@Path("/retrieveAccid")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveAccidByDoctorCode(@QueryParam("doctorCode")String doctorCode);

	void retrieveAccidByDoctorCode(String doctorCode, HttpServletRequest request, HttpServletResponse response);
	
	/**
	 * 根据拼音查询医生
	 * @param namePinYin
	 * @return
	 */
	@GET
	@Path("/retrieveDoctorByPinYin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveDoctorByPinYin(@QueryParam("namePy")String namePy,@QueryParam("curpage")int curpage,@QueryParam("pageSize")int pageSize);
	
	@GET
	@Path("retrieveAllDoctorByName")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> retrieveAllDoctorByName(@QueryParam("name")String name);
}
