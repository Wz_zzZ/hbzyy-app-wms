package com.sinosoft.hbzyy.app.wms.base.data.endpoint.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;

import com.sinosoft.hbzyy.app.wms.base.data.api.DepartmentService;
import com.sinosoft.hbzyy.app.wms.base.data.api.ParentDeptService;
import com.sinosoft.hbzyy.app.wms.base.data.endpoint.DepartmentEndpoint;
import com.sinosoft.hbzyy.app.wms.base.data.model.DepartmentEntity;
import com.sinosoft.hbzyy.app.wms.base.data.model.ParentDeptEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;
import com.sinosoft.hbzyy.framework.util.ResultHelper;

public class DepartmentEndpointImpl implements DepartmentEndpoint {
	
	private DepartmentService departmentService;
	
	private ParentDeptService parentDeptService;
	

	public ParentDeptService getParentDeptService() {
		return parentDeptService;
	}

	public void setParentDeptService(ParentDeptService parentDeptService) {
		this.parentDeptService = parentDeptService;
	}

	public DepartmentService getDepartmentService() {
		return departmentService;
	}

	public void setDepartmentService(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}

	@Override
	public Map<String, Object> createDepartment(List<DepartmentEntity> departs) {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);// 获取到request
		for(DepartmentEntity depart:departs) {
			depart.setCreateBy(Long.parseLong(request.getSession().getAttribute("username").toString()));
		}
		try {
			return departmentService.createDepartment(departs);
		} catch (IOException | RuntimeException e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult(e.getMessage());
		}
	}

	@Override
	public Map<String, Object> updateDepartment(DepartmentEntity depart) {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);// 获取到request
		depart.setCreateBy(Long.parseLong(request.getSession().getAttribute("username").toString()));
		return departmentService.updateDepartment(depart);
	}

	@Override
	public Map<String,Object> retrieveDepartment(String id,String orgId,String orgName,String orgCode,String deptName,String parentCode,int currentPage,int rows ) {
		List<DepartmentEntity> pageList=departmentService.retrieveDepartment(id,orgId,orgName,orgCode,deptName,parentCode, currentPage, rows);
		return ResultHelper.buildSuccessResult(pageList);
	}

	@Override
	public Map<String, Object> updateDepartmentByDeptCode(String deptCode, String status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getOrgNameByOrgCode(String orgCode) {
		return departmentService.getOrgNameByOrgCode(orgCode);
	}

	@Override
	public Map<String, Object> deleteDeptById(String id) {
		return departmentService.deleteDepartmentById(id);
	}

	@Override
	public Map<String, Object> createParentDept(ParentDeptEntity parentDept) {
		return parentDeptService.createParentDept(parentDept);
	}

	@Override
	public Map<String, Object> updateParentDept(ParentDeptEntity parentDept) {
		return parentDeptService.updateParentDept(parentDept);

	}

	@Override
	public Map<String, Object> retrieveParentDept(String orgCode, String code, String status) {
		return parentDeptService.retrieveParentDept(orgCode, code, status);
	}

	@Override
	public Map<String, Object> deleteParentDept(String id) {
		return parentDeptService.deleteParentDept(id);
	}

	@Override
	public Map<String, Object> retrieveDeparts(String orgId, String orgCode,String deptName, int currentPage, int rows) {
		return departmentService.retrieveDepartments(orgId,orgCode,deptName,currentPage, rows);
		
	}

	@Override
	public Map<String, Object> retrieveAllDeparts(String orgCode,String orgId,String deptName) {
		return departmentService.retrieveAllDept(orgCode, orgId,deptName);
	}

	@Override
	public Map<String, Object> retrieveDepart(String orgCode, String orgId, String deptName) {
		return departmentService.retrieveDepart(orgCode, orgId, deptName);
	}

}
