package com.sinosoft.hbzyy.app.wms.doctorteam.mc.model;

import java.io.Serializable;
import java.util.List;

public class ExamRecordSummaryEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2475535853440004447L;
	/**
	 * 团体名称
	 */
	private String orgName;
	/**
	 * 套餐名称
	 */
	private String examSuiteName;
	/**
	 * 体检代码集合
	 */
	private List<PatientInfoEntity> codeList;
	
	private Integer flag;
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getExamSuiteName() {
		return examSuiteName;
	}
	public void setExamSuiteName(String examSuiteName) {
		this.examSuiteName = examSuiteName;
	}
	public List<PatientInfoEntity> getCodeList() {
		return codeList;
	}
	public void setCodeList(List<PatientInfoEntity> codeList) {
		this.codeList = codeList;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	
	
}
