package com.sinosoft.hbzyy.app.wms.doctorteam.mc.model;

import java.io.Serializable;

public class ExamRecordSummaryFilter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8874841593545985741L;
	private String orgId;
	private String orgName;
	private String examSuiteId;
	private String examSuiteName;
	private String startTime;
	private String endTime;
	private Integer flag;
	private Integer num;
	
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getExamSuiteId() {
		return examSuiteId;
	}
	public void setExamSuiteId(String examSuiteId) {
		this.examSuiteId = examSuiteId;
	}
	public String getExamSuiteName() {
		return examSuiteName;
	}
	public void setExamSuiteName(String examSuiteName) {
		this.examSuiteName = examSuiteName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	
}
