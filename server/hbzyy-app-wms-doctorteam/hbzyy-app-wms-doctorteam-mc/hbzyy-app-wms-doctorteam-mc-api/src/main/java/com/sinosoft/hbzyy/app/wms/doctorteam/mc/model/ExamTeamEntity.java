package com.sinosoft.hbzyy.app.wms.doctorteam.mc.model;

import java.io.Serializable;
/**
 * 体检团体实体类
 * @author tsy94
 *
 */
public class ExamTeamEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7098160399679523960L;
	/**
	 * 主键id
	 */
	private Long id;
	/**
	 * 团体代码
	 */
	private String orgCode;
	/**
	 * 团体名称
	 * 
	 */
	private String orgName;
	/**
	 * 团体全称
	 */
	private String orgFullName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgFullName() {
		return orgFullName;
	}
	public void setOrgFullName(String orgFullName) {
		this.orgFullName = orgFullName;
	}
	public Long getOrgClass() {
		return orgClass;
	}
	public void setOrgClass(Long orgClass) {
		this.orgClass = orgClass;
	}
	/**
	 * 团体类型
	 */
	private Long orgClass;
}
