package com.sinosoft.hbzyy.app.wms.doctorteam.mc.api;

import java.util.List;
import java.util.Map;

import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamRecordSummaryFilter;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamSuitesEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.PatientInfoEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;

/**
 * 后台管理体检业务接口
 * @author tsy94
 *
 */
public interface ExamService {
	/**
	 * 根据团体名称模糊搜索体检团体
	 * @param keyword
	 * @return
	 */
	public List<ExamTeamEntity> getExamTeamsByKeyWord(String keyword);
	
	/**
	 * 根据套餐名称模糊搜索体检套餐
	 * @param keyword
	 * @return
	 */
	public List<ExamSuitesEntity> getExamExamSuitesByKeyWord(String keyword);
	
	/**
	 * 根据条件查询体检记录
	 * @param param  orgName团队名称,examSutieName 套餐名称,startTime,endTime
	 * @return
	 */
	public PageList<PatientInfoEntity> getExamRecordByParam(Map<String,Object> param);
	
	/**
	 * 查询团体或套餐过滤器状态
	 * @param esf
	 * @return
	 */
	public ExamRecordSummaryFilter getSummaryEntity(ExamRecordSummaryFilter esf);
	
	/**
	 * 根据体检编号查询体检记录的状态
	 * @param id
	 * @return
	 */
	public Integer getDisableByPatientId(String id);
	
	/**
	 * 新增禁止查阅的体检报告(个人)
	 * @param id
	 */
	public void addNewDisableRecord(PatientInfoEntity id);
	
	/**
	 * 解禁旧的体检报告
	 * @param id
	 */
	public void deleteOldDisableRecord(PatientInfoEntity pie);
	
	/**
	 * 增加新过滤条件
	 * @param esf
	 */
	public void addNewFilter(ExamRecordSummaryFilter esf);
	
	/**
	 * 删除旧过滤条件
	 * @param esf
	 */
	public void deleteOldFilter(ExamRecordSummaryFilter esf);
}
