package com.sinosoft.hbzyy.app.wms.doctorteam.mc.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 患者基本信息实体
 * @author tsy94
 *
 */
public class PatientInfoEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1327064165158584020L;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 性别
	 */
	private String gender;
	/**
	 * 年龄
	 */
	private Integer age; 
	/**
	 * 登记日期
	 */
	private Date regDate;
	/**
	 * 健康档案号码
	 */
	private String patientId;
	/**
	 * 体检号码
	 */
	private String patientCode;
	
	/**
	 * 是否可以查阅,默认0 可以查阅
	 */
	private Integer isAble = 0; 
	
	private String orgName;
	
	private String orgId;
	
	private String examSuiteId;
	
	private String suiteName;
	
	private Date createDate;
	
	public String getExamSuiteId() {
		return examSuiteId;
	}
	public void setExamSuiteId(String examSuiteId) {
		this.examSuiteId = examSuiteId;
	}
	
	public Integer getIsAble() {
		return isAble;
	}
	public void setIsAble(Integer isAble) {
		this.isAble = isAble;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getSuiteName() {
		return suiteName;
	}
	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getPatientCode() {
		return patientCode;
	}
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
}
