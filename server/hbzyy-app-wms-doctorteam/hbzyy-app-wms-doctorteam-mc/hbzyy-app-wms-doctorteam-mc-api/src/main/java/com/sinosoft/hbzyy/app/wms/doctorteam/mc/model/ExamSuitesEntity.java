package com.sinosoft.hbzyy.app.wms.doctorteam.mc.model;

import java.io.Serializable;

public class ExamSuitesEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -759830704039090672L;
	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 套餐名称
	 */
	private String suiteName;
	/**
	 * 套餐代码
	 */
	private String suiteCode;
	/**
	 * 价格
	 */
	private Double price;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSuiteName() {
		return suiteName;
	}
	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}
	public String getSuiteCode() {
		return suiteCode;
	}
	public void setSuiteCode(String suiteCode) {
		this.suiteCode = suiteCode;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
}
