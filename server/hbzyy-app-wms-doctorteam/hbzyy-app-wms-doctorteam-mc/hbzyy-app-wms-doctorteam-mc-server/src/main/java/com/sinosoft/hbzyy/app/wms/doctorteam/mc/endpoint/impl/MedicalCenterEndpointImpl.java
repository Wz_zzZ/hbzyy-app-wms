package com.sinosoft.hbzyy.app.wms.doctorteam.mc.endpoint.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sinosoft.hbzyy.app.wms.doctorteam.mc.api.ExamService;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.endpoint.MedicalCenterEndpoint;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamRecordSummaryFilter;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamSuitesEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.PatientInfoEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;
import com.sinosoft.hbzyy.framework.util.ResultHelper;

public class MedicalCenterEndpointImpl implements MedicalCenterEndpoint {
	private static final Logger logger = Logger.getLogger(MedicalCenterEndpointImpl.class);
	private ExamService examService;
	public ExamService getExamService() {
		return examService;
	}

	public void setExamService(ExamService examService) {
		this.examService = examService;
	}

	@Override
	public Map<String, Object> getExamTeamsByKeyWord(String keyword) {
		try {
			List<ExamTeamEntity> list = examService.getExamTeamsByKeyWord(keyword);
			return ResultHelper.buildSuccessResult(list);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("查询失败,请稍候再试");
		}
		
		
	}

	@Override
	public Map<String, Object> getExamExamSuitesByKeyWord(String keyword) {
		try {
			List<ExamSuitesEntity> list = examService.getExamExamSuitesByKeyWord(keyword);
			return ResultHelper.buildSuccessResult(list);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("查询失败,请稍候再试");
		}
	}

	@Override
	public Map<String, Object> getExamRecordByParam(Map<String, Object> param) {
		String isLoad = (String) param.get("isLoad");
		if(isLoad.equals("1")) {
			return ResultHelper.buildSuccessResult(new ArrayList());
		}
		try {
			PageList<PatientInfoEntity> list = examService.getExamRecordByParam(param);
			return ResultHelper.buildSuccessResult(list);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("查询失败,请稍候再试");
		}
	}

	@Override
	public Map<String, Object> addNewDisableRecord(PatientInfoEntity pie) {
		try {
			examService.addNewDisableRecord(pie);
			return ResultHelper.buildSuccessResult("禁用成功");
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("添加禁用失败,请稍候再试");
		}
	}

	@Override
	public Map<String, Object> deleteOldDisableRecord(PatientInfoEntity pie) {
		try {
			examService.deleteOldDisableRecord(pie);
			return ResultHelper.buildSuccessResult("解禁成功");
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("解除禁用失败,请稍候再试");
		}
	}

	@Override
	public Map<String, Object> addNewFilter(ExamRecordSummaryFilter esf) {
		try {
			examService.addNewFilter(esf);
			return ResultHelper.buildSuccessResult("禁用成功");
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("添加禁用失败,请稍候再试");
		}
	}

	@Override
	public Map<String, Object> deleteOldFilter(ExamRecordSummaryFilter esf) {
		try {
			examService.deleteOldFilter(esf);
			return ResultHelper.buildSuccessResult("解禁成功");
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("解除禁用失败,请稍候再试");
		}
	}

	@Override
	public Map<String, Object> getSummaryEntity(ExamRecordSummaryFilter esf) {
		try {
			return ResultHelper.buildSuccessResult(examService.getSummaryEntity(esf));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResultHelper.buildErrorResult("查询失败,请稍候再试");
		}
	}

}
