package com.sinosoft.hbzyy.app.wms.doctorteam.mc.endpoint;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamRecordSummaryFilter;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.PatientInfoEntity;


public interface MedicalCenterEndpoint {
	
	/**
	 * 查询团队信息
	 * @param keyword
	 * @return
	 */
	@GET
	@Path("/examteams")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> getExamTeamsByKeyWord(@QueryParam("keyword")String keyword);
	
	/**
	 * 查询套餐信息
	 * @param keyword
	 * @return
	 */
	@GET
	@Path("/examsuites")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> getExamExamSuitesByKeyWord(@QueryParam("keyword")String keyword);
	
	/**
	 * 查询体检记录
	 * @param keyword
	 * @return
	 */
	@POST
	@Path("/examrecords")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> getExamRecordByParam(Map<String,Object> keyword);
	
	/**
	 * 单个禁用
	 * @param id
	 * @return
	 */
	@POST
	@Path("/disable")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> addNewDisableRecord(PatientInfoEntity pie);
	
	/**
	 * 单个启用
	 * @param codeList
	 * @return
	 */
	@POST
	@Path("/able")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> deleteOldDisableRecord(PatientInfoEntity pie);
	
	/**
	 * 全部禁用
	 * @param esf
	 * @return
	 */
	@POST
	@Path("/summary/disable")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> addNewFilter(ExamRecordSummaryFilter esf);
	
	/**
	 * 全部启用
	 * @param esf
	 * @return
	 */
	@POST
	@Path("/summary/able")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> deleteOldFilter(ExamRecordSummaryFilter  esf);
	
	@POST
	@Path("/summary")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> getSummaryEntity(ExamRecordSummaryFilter  esf);
}
