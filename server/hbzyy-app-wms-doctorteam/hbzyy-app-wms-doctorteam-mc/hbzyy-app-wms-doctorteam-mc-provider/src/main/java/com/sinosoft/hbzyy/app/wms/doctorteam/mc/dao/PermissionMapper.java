package com.sinosoft.hbzyy.app.wms.doctorteam.mc.dao;

import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamRecordSummaryFilter;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.PatientInfoEntity;

public interface PermissionMapper {
	/**
	 * 根据体检编号查询是否被禁止查阅
	 * @param id
	 * @return
	 */
	public Integer getDisableByPatientId(String id);
	
	/**
	 * 新增禁止查阅的体检报告
	 * @param id
	 */
	public void addNewDisableRecord(PatientInfoEntity id);
	
	/**
	 * 更改体检报告状态
	 * @param pie
	 */
	public void updateFlagByPatientCode(PatientInfoEntity pie);
	
	/**
	 * 查询团体或套餐的禁启用状态
	 * @param esf
	 */
	public Integer getFilterState(ExamRecordSummaryFilter esf);
	
	/**
	 * 新增团体或套餐禁用
	 * @param esf
	 */
	public void addNewFilter(ExamRecordSummaryFilter esf);
	
	/**
	 * 团体或套餐解禁
	 * @param esf
	 */
	public void deleteOldFilter(ExamRecordSummaryFilter esf);
	
	/**
	 * 根据团体或者套餐从中间表中删除个人的记录
	 * 此方法用于在解禁团体或套餐时,连带解禁个人记录,无论该个人是否为禁用状态
	 * 其目的是为了防止用户在多次操作团体和个人的启用禁用状态后,出现难以找到个人记录的情况
	 * @param esf
	 */
	public void deleteOldBanByFilter(ExamRecordSummaryFilter esf);
}
