package com.sinosoft.hbzyy.app.wms.doctorteam.mc.dao;

import java.util.List;
import java.util.Map;

import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamSuitesEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.PatientInfoEntity;

public interface ExamInfoMapper {
	/**
	 * 根据团体名称模糊搜索体检团体
	 * @param keyword
	 * @return
	 */
	public List<ExamTeamEntity> getExamTeamsByKeyWord(String keyword);
	
	/**
	 * 根据套餐名称模糊搜索体检套餐
	 * @param keyword
	 * @return
	 */
	public List<ExamSuitesEntity> getExamExamSuitesByKeyWord(String keyword);
	
	/**
	 * 根据条件查询体检记录
	 * @param param  patientName 患者姓名,orgName团队名称,examSutieName 套餐名称,
	 * patientCode 体检号,
	 * startTime,endTime
	 * @return
	 */
	public List<PatientInfoEntity> getExamRecordByParam(Map<String,Object> param);
	
	public Integer countExamRecordByParam(Map<String,Object> param);
}
