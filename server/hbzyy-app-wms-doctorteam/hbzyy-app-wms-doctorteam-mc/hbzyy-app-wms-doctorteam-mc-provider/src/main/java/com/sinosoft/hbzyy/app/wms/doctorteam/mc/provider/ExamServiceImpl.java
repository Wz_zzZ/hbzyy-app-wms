package com.sinosoft.hbzyy.app.wms.doctorteam.mc.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sinosoft.hbzyy.app.wms.doctorteam.mc.api.ExamService;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.dao.ExamInfoMapper;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.dao.PermissionMapper;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamRecordSummaryFilter;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamSuitesEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.ExamTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.mc.model.PatientInfoEntity;
import com.sinosoft.hbzyy.framework.core.api.model.PageList;

public class ExamServiceImpl implements ExamService{
	private ExamInfoMapper examInfoMapper;
	
	private PermissionMapper permissionMapper;
	
	public ExamInfoMapper getExamInfoMapper() {
		return examInfoMapper;
	}

	public void setExamInfoMapper(ExamInfoMapper examInfoMapper) {
		this.examInfoMapper = examInfoMapper;
	}

	public PermissionMapper getPermissionMapper() {
		return permissionMapper;
	}

	public void setPermissionMapper(PermissionMapper permissionMapper) {
		this.permissionMapper = permissionMapper;
	}

	@Override
	public List<ExamTeamEntity> getExamTeamsByKeyWord(String keyword) {
		return examInfoMapper.getExamTeamsByKeyWord(keyword);
	}

	@Override
	public List<ExamSuitesEntity> getExamExamSuitesByKeyWord(String keyword) {
		return examInfoMapper.getExamExamSuitesByKeyWord(keyword);
	}

	@Override
	public PageList<PatientInfoEntity> getExamRecordByParam(Map<String, Object> param) {
		if(param.get("page") == null || param.get("rows") == null) {
			param.put("page",1);
			param.put("rows",10);
		}
		List<PatientInfoEntity> datas = examInfoMapper.getExamRecordByParam(param);
		for(PatientInfoEntity pie : datas) {
			ExamRecordSummaryFilter esf = new ExamRecordSummaryFilter();
			esf.setOrgId(pie.getOrgId());
			esf.setExamSuiteId(pie.getExamSuiteId());
			Integer flag = permissionMapper.getDisableByPatientId(pie.getPatientCode());
			Integer state = permissionMapper.getFilterState(esf);
			if(flag == null && state == 0) {
				pie.setIsAble(0);
			}
			if(flag == null && state == 1) {
				pie.setIsAble(1);
			}
			if(flag != null) {
				if(flag == 1) {
					pie.setIsAble(1);
				}
				if(flag == 0) {
					pie.setIsAble(0);
				}
			}
			
		}
		Integer total = examInfoMapper.countExamRecordByParam(param);
		PageList<PatientInfoEntity> pageList = new PageList<>();
		pageList.setDatas(datas);
		pageList.setPageNum(Integer.valueOf(param.get("page").toString()));
		pageList.setPageSize(Integer.valueOf(param.get("rows").toString()));
		pageList.setTotal(total);
		return pageList;
	}

	@Override
	public Integer getDisableByPatientId(String id) {
		return permissionMapper.getDisableByPatientId(id);
	}

	@Override
	public void addNewDisableRecord(PatientInfoEntity pie) {
		Integer flag = permissionMapper.getDisableByPatientId(pie.getPatientCode());
		if(flag == null) {
			pie.setIsAble(1);
			permissionMapper.addNewDisableRecord(pie);
		}else {
			pie.setIsAble(1);
			permissionMapper.updateFlagByPatientCode(pie);
		}
	}

	@Override
	public void deleteOldDisableRecord(PatientInfoEntity pie) {
		Integer flag = permissionMapper.getDisableByPatientId(pie.getPatientCode());
		if(flag == null) {
			pie.setIsAble(0);
			permissionMapper.addNewDisableRecord(pie);
		}else {
			pie.setIsAble(0);
			permissionMapper.updateFlagByPatientCode(pie);
		}
	}

	@Override
	public void addNewFilter(ExamRecordSummaryFilter esf) {
		permissionMapper.addNewFilter(esf);
	}

	@Override
	public void deleteOldFilter(ExamRecordSummaryFilter esf) {
		permissionMapper.deleteOldFilter(esf);
		permissionMapper.deleteOldBanByFilter(esf);
		
	}
	
	@Override
	public ExamRecordSummaryFilter getSummaryEntity(ExamRecordSummaryFilter esf) {
		esf.setFlag(permissionMapper.getFilterState(esf));
		Map<String,Object> param = new HashMap<>();
		param.put("orgId", esf.getOrgId());
		param.put("examSuiteId", esf.getExamSuiteId());
		esf.setNum(examInfoMapper.countExamRecordByParam(param));
		return esf;
	}
	
	/**
	 * 对象集合拆分工具
	 * @param objList 对象集合
	 * @param size 单个集合的长度
	 * @return 拆分后的集合的集合
	 */
	private static <T> List<List<T>> subList(List<T> objList, int size) {
		List<List<T>> list = new ArrayList<List<T>>();
		//不够分组直接返回
		if (objList.size() <= size) {
			list.add(objList);
			return list;
		}
		//计算组数和余数
		int groupAmount = objList.size() / size;
		int leftAmount = objList.size() % size;
		@SuppressWarnings("unchecked")
		//将原集合转换成数组
		T[] arry = (T[]) objList.toArray();
		//添加分组集合
		for (int i = 0; i < groupAmount * size; i += size ) {
			list.add(Arrays.asList((Arrays.copyOfRange(arry, i, size + i))));
		}
		//添加余数集合
		if(leftAmount != 0) {
			list.add(Arrays.asList(Arrays.copyOfRange(arry, groupAmount * size, objList.size())));
		}
		return list;
	}
}
