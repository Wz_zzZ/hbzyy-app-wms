package com.sinosoft.hbzyy.app.wms.docotorteam.common.endpoint.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.apache.log4j.Logger;

import com.google.zxing.WriterException;
import com.sinosoft.hbzyy.app.weedfs.api.WeedfsService;
import com.sinosoft.hbzyy.app.weedfs.model.RequestResult;
import com.sinosoft.hbzyy.app.wms.docotorteam.common.endpoint.CommonEndpoint;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.api.CommonServices;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorInfoEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.TeamPicEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.util.QRCodeUtil;
import com.sinosoft.hbzyy.framework.util.ResultHelper;
import com.sinosoft.hbzyy.framework.util.StringUtil;

public class CommonEndpointImpl implements CommonEndpoint {
	public static final Logger logger = Logger.getLogger(CommonEndpointImpl.class);
	private CommonServices commonServices;
	private WeedfsService weedfsService;

	public WeedfsService getWeedfsService() {
		return weedfsService;
	}

	public void setWeedfsService(WeedfsService weedfsService) {
		this.weedfsService = weedfsService;
	}

	@Override
	public Map<String, Object> getTeamList() {
		try {
			List<DoctorTeamEntity> list = commonServices.getTeamList();
			return ResultHelper.buildSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("获取医生团队列表失败,请稍候重试");
		}
	}

	@Override
	public Map<String, Object> updateTeamInfo(DoctorTeamEntity team) {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);// 获取到request
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		System.out.println(username);
		team.setUpdateBy(Long.valueOf(username));
		try {
			commonServices.updateTeamInfoById(team);
			addQRCodeImgURl(team.getId(), 900, "png", team.getId(), team.getQRcode());
			return ResultHelper.buildSuccessResult("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("保存失败");
		}
	}

	@Override
	public Map<String, Object> getTeamInfo(String id) {
		try {
			DoctorTeamEntity teamInfo = commonServices.getTeamInfoById(id);
			if (!StringUtil.isBlank(teamInfo.getMemberNums())) {
				String[] names = teamInfo.getMemberNums().split(",");
				List<DoctorInfoEntity> doctorNames = commonServices.getDoctorNames(names);
				teamInfo.setDoctorNames(doctorNames);
			}
			return ResultHelper.buildSuccessResult(teamInfo);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("获取医生团队信息失败,请稍候重试");
		}
	}

	@Override
	public Map<String, Object> uploadTeamPic(TeamPicEntity teamPic) {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);// 获取到request
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		teamPic.setCreateBy(Long.valueOf(username));
		teamPic.setUpdateBy(Long.valueOf(username));
		// 上传图片
		try {
			commonServices.addTeamPic(teamPic);
			return ResultHelper.buildSuccessResult("上传成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("上传图片失败");
		}

	}

	@Override
	public Map<String, Object> changeTeamOrder(List<DoctorTeamEntity> team) {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);// 获取到request
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		DoctorTeamEntity team1 = team.get(0);
		DoctorTeamEntity team2 = team.get(1);
		team1.setUpdateBy(Long.valueOf(username));
		team2.setUpdateBy(Long.valueOf(username));
		Integer order1 = team1.getOrderId();
		Integer order2 = team2.getOrderId();
		team1.setOrderId(order2);
		team2.setOrderId(order1);
		try {
			commonServices.updateTeamInfoById(team1);
			commonServices.updateTeamInfoById(team2);
			return ResultHelper.buildSuccessResult("交换成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("交换失败");
		}
	}

	public CommonServices getCommonServices() {
		return commonServices;
	}

	public void setCommonServices(CommonServices commonServices) {
		this.commonServices = commonServices;
	}

	private void addQRCodeImgURl(String content, int size, String format, String teamId, String qrUrl)
			throws WriterException, IOException {
		if (StringUtil.isBlank(qrUrl)) {
			weedfsService.deleteFile(qrUrl);
		}
		DoctorTeamEntity team = new DoctorTeamEntity();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		QRCodeUtil.createQrCode(out, content, size, format);
		RequestResult rr = weedfsService.uploadFiles(out.toByteArray());
		team.setQRcode(rr.getFid());
		team.setId(teamId);
		commonServices.updateTeamInfoById(team);
	}

	@Override
	public Map<String, Object> deleteOldPic(String id,boolean flag) {
		String url = "";
		try {
			if(flag) {
				url = commonServices.getPicById(id).getPicUrl();
				commonServices.deleteOldPic(id);
			}else {
				url = id;
			}
			weedfsService.deleteFile(url);
			return ResultHelper.buildSuccessResult("删除成功");
		} catch (Exception e) {

			return ResultHelper.buildErrorResult("删除失败");
		}
	}

	@Override
	public Map<String, Object> getObesityTeamTags() {
		try {
			return ResultHelper.buildSuccessResult(commonServices.getObesityTeamTags());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultHelper.buildErrorResult("查询失败,请稍候重试");
		}
	}

}
