package com.sinosoft.hbzyy.app.wms.docotorteam.common.endpoint;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.TeamPicEntity;

public interface CommonEndpoint {
	
	/**
	 * 获取医生团队列表
	 * @return
	 */
	@GET
	@Path("/teams")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> getTeamList();
	
	/**
	 * 更新医生团队信息
	 * @return
	 */
	@POST
	@Path("/updateInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> updateTeamInfo(DoctorTeamEntity team);
	
	/**
	 * 根据id获取医生团队信息
	 * @param id
	 * @return
	 */
	@GET
	@Path("/teamInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> getTeamInfo(@QueryParam("id")String id);
	
	/**
	 * 上传团队图片+描述
	 * @param pic
	 * @return
	 */
	@POST
	@Path("/teampic")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> uploadTeamPic(TeamPicEntity teamPic); 
	
	/**
	 * 切换排序
	 * @param team1
	 * @param team2
	 * @return
	 */
	@POST
	@Path("/order")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> changeTeamOrder(List<DoctorTeamEntity> team);
	
	@GET
	@Path("/delpic")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> deleteOldPic(@QueryParam("id")String id,@QueryParam("flag")boolean flag);
	
	@GET
	@Path("/tags")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,Object> getObesityTeamTags();
}
