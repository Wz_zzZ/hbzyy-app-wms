package com.sinosoft.hbzyy.app.wms.doctorteam.common.model;

import java.io.Serializable;

public class DoctorInfoEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2956686552366786661L;
	private String name;
	private String doctorCode;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDoctorCode() {
		return doctorCode;
	}
	public void setDoctorCode(String doctorCode) {
		this.doctorCode = doctorCode;
	}
	
}
