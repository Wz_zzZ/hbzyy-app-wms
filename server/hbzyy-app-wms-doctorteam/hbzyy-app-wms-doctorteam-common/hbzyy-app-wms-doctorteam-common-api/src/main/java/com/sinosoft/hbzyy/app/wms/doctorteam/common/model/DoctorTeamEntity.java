package com.sinosoft.hbzyy.app.wms.doctorteam.common.model;

import java.util.List;
import java.util.Map;

import com.sinosoft.hbzyy.framework.core.api.model.BaseVO;

public class DoctorTeamEntity extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1051806684773063203L;
	/**
	 * 主键id
	 */
	private String id;
	/**
	 * 所属机构id
	 */
	private Long orgId;
	/**
	 * 所属机构名称
	 */
	private String orgName;
	/**
	 * 所属科室id
	 */
	private String deptId;
	/**
	 * 所属科室名称
	 */
	private String deptName;
	/**
	 * 团队名称
	 */
	private String name;
	/**
	 * 擅长领域
	 */
	private String goodAt;
	/**
	 * 团队描述
	 */
	private String desc;
	/**
	 * 关注数
	 */
	private Integer follows;
	/**
	 * 成员人数
	 */
	private Integer members;
	/**
	 * 团队成员工号
	 */
	private String memberNums;
	/**
	 * 状态
	 */
	private String status;
	
	/**
	 * 二维码地址
	 */
	private String QRcode;
	
	/**
	 * 图片列表
	 */
	private List<TeamPicEntity> picList; 
	
	/**
	 * 团队医生姓名集合
	 */
	private List<DoctorInfoEntity> doctorNames;
	
	/**
	 * 排序号
	 */
	private Integer orderId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGoodAt() {
		return goodAt;
	}
	public void setGoodAt(String goodAt) {
		this.goodAt = goodAt;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Integer getFollows() {
		return follows;
	}
	public void setFollows(Integer follows) {
		this.follows = follows;
	}
	public Integer getMembers() {
		return members;
	}
	public void setMembers(Integer members) {
		this.members = members;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getQRcode() {
		return QRcode;
	}
	public void setQRcode(String qRcode) {
		QRcode = qRcode;
	}
	public String getMemberNums() {
		return memberNums;
	}
	public void setMemberNums(String memberNums) {
		this.memberNums = memberNums;
	}
	public List<TeamPicEntity> getPicList() {
		return picList;
	}
	public void setPicList(List<TeamPicEntity> picList) {
		this.picList = picList;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public List<DoctorInfoEntity> getDoctorNames() {
		return doctorNames;
	}
	public void setDoctorNames(List<DoctorInfoEntity> doctorNames) {
		this.doctorNames = doctorNames;
	}
	
}
