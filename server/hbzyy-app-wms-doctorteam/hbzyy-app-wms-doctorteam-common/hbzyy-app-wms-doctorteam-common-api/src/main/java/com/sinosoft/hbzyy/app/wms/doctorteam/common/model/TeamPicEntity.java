package com.sinosoft.hbzyy.app.wms.doctorteam.common.model;

import com.sinosoft.hbzyy.framework.core.api.model.BaseVO;

public class TeamPicEntity extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8310832281217039070L;
	/**
	 * 主键id
	 */
	private String id;
	/**
	 * 团队id
	 */
	private String teamId;
	/**
	 * 图片地址
	 */
	private String picUrl;
	/**
	 * 图片描述
	 */
	private String picDesc;
	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getPicDesc() {
		return picDesc;
	}
	public void setPicDesc(String picDesc) {
		this.picDesc = picDesc;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
