package com.sinosoft.hbzyy.app.wms.doctorteam.common.api;

import java.util.List;

import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorInfoEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.TeamPicEntity;
import com.sinosoft.hbzyy.app.wms.tags.model.TagsEntity;

public interface CommonServices {
	
	/**
	 * 获取团队列表
	 * @return
	 */
	public List<DoctorTeamEntity> getTeamList();
	
	/**
	 * 修改团队信息
	 * @param team
	 */
	public void updateTeamInfoById(DoctorTeamEntity team);
	
	/**
	 * 根据id获取团队详细信息
	 * @param id
	 * @return
	 */
	public DoctorTeamEntity getTeamInfoById(String id);
	
	/**
	 * 增加团队图片信息
	 * @param teamPic
	 */
	public void addTeamPic(TeamPicEntity teamPic);
	
	/**
	 * 根据Id获取图片信息
	 * @param id
	 * @return
	 */
	public TeamPicEntity getPicById(String id);
	
	/**
	 * 删除旧图片
	 * @param id
	 */
	public void deleteOldPic(String id);
	
	/**
	 * 获取团队医生信息
	 * @param codes
	 * @return
	 */
	public List<DoctorInfoEntity> getDoctorNames(String[] codes);
	
	/**
	 * 获取肥胖课所有分类标签
	 * @return
	 */
	public List<TagsEntity> getObesityTeamTags();

}
