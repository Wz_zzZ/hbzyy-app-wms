package com.sinosoft.hbzyy.app.wms.doctorteam.common.dao;

import java.util.List;

import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorInfoEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.TeamPicEntity;
import com.sinosoft.hbzyy.app.wms.tags.model.TagsEntity;

public interface CommonMapper {
	
	/**
	 * 获取团队列表
	 * @return
	 */
	public List<DoctorTeamEntity> getTeamList();
	
	/**
	 * 更新团队信息
	 * @param team
	 */
	public void updateTeamInfoById(DoctorTeamEntity team);
	
	/**
	 * 根据id获取团队信息
	 * @param id
	 * @return
	 */
	public DoctorTeamEntity getTeamInfoById(String id);
	
	/**
	 * 根据teamId获取图片列表
	 * @return
	 */
	public List<TeamPicEntity> getPicListByTeamId(String id); 
	
	/**
	 * 添加团队图片
	 * @param pic
	 */
	public void addTeamPic(TeamPicEntity pic);
	
	/**
	 * 更新图片
	 * @param pic
	 */
	public void updateTeamPic(TeamPicEntity pic);
	
	/**
	 * 根据id获取图片信息
	 * @param id
	 * @return
	 */
	public TeamPicEntity getPicById(String id);
	
	/**
	 * 获取团队医生姓名
	 * @param names
	 * @return
	 */
	public List<DoctorInfoEntity> getDoctorNames(String[] names);
	
	/**
	 * 删除旧图片
	 * @param id
	 */
	public void deleteOldPic(String id);
	
	/**
	 * 获取肥胖科所有分类标签
	 * @return
	 */
	public List<TagsEntity> getObesityteamTags();

}
