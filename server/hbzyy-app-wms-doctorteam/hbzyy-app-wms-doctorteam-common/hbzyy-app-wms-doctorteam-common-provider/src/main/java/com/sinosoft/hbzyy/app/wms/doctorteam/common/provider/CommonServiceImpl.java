package com.sinosoft.hbzyy.app.wms.doctorteam.common.provider;

import java.util.List;
import java.util.UUID;

import com.sinosoft.hbzyy.app.wms.doctorteam.common.api.CommonServices;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.dao.CommonMapper;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorInfoEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.DoctorTeamEntity;
import com.sinosoft.hbzyy.app.wms.doctorteam.common.model.TeamPicEntity;
import com.sinosoft.hbzyy.app.wms.tags.model.TagsEntity;

public class CommonServiceImpl implements CommonServices {
	private CommonMapper commonMapper;

	public CommonMapper getCommonMapper() {
		return commonMapper;
	}

	public void setCommonMapper(CommonMapper commonMapper) {
		this.commonMapper = commonMapper;
	}

	@Override
	public List<DoctorTeamEntity> getTeamList() {
		return commonMapper.getTeamList();
	}

	@Override
	public void updateTeamInfoById(DoctorTeamEntity team) {
		 commonMapper.updateTeamInfoById(team);
	}

	@Override
	public DoctorTeamEntity getTeamInfoById(String id) {
		DoctorTeamEntity team = commonMapper.getTeamInfoById(id);
		return team;
	}

	@Override
	public void addTeamPic(TeamPicEntity teamPic) {
		teamPic.setId(UUID.randomUUID().toString());
		commonMapper.addTeamPic(teamPic);
	}

	@Override
	public TeamPicEntity getPicById(String id) {
		return commonMapper.getPicById(id);
	}

	@Override
	public void deleteOldPic(String id) {
		commonMapper.deleteOldPic(id);
	}
	
	public static void main(String[] args) {
		System.out.println(UUID.randomUUID().toString());
	}

	@Override
	public List<DoctorInfoEntity> getDoctorNames(String[] codes) {
		return commonMapper.getDoctorNames(codes);
	}

	@Override
	public List<TagsEntity> getObesityTeamTags() {
		return commonMapper.getObesityteamTags();
	}

}
